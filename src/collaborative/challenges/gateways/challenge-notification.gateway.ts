import {
    ConnectedSocket,
    MessageBody,
    OnGatewayConnection,
    SubscribeMessage,
    WebSocketGateway,
    WebSocketServer,
} from '@nestjs/websockets';
import { Server, Socket } from 'socket.io';
import { User } from 'src/users/entities/user.entity';
import { AuthService } from '../../../auth/auth.service';
import { GetChallengeDto } from '../dto/get-challenge.dto';

@WebSocketGateway({
    cors: {
        origin: '*',
    },
})
export class ChallengeGateway implements OnGatewayConnection {
    @WebSocketServer()
    server: Server;

    // Store connected users
    private connectedUsers: {
        socketId: string;
        user: User;
    }[] = [];

    constructor(private readonly authService: AuthService) {}

    async handleConnection(client: any, ...args: any[]) {
        const foundUserFromSocket = await this.authService.getUserFromSocket(
            client,
        );
        if (foundUserFromSocket) {
            if (
                !this.connectedUsers.find(
                    (connectedUser) =>
                        connectedUser.user.id === foundUserFromSocket.id,
                )
            ) {
                this.connectedUsers.push({
                    socketId: client.id,
                    user: await this.authService.getUserFromSocket(client),
                });
                console.log(
                    'Client connected: ',
                    client.id,
                    'with user: ',
                    foundUserFromSocket.email,
                );
            } else {
                console.log(
                    'Client already connected: ',
                    client.id,
                    'with user: ',
                    foundUserFromSocket.email,
                );
                // Replace socket id if user is already connected
                this.connectedUsers = this.connectedUsers.map((connectedUser) =>
                    connectedUser.user.id === foundUserFromSocket.id
                        ? {
                              socketId: client.id,
                              user: connectedUser.user,
                          }
                        : connectedUser,
                );
            }
        }
    }

    @SubscribeMessage('send_challenge')
    async listenForChallengesSent(
        @MessageBody() challenge: { data: string },
        @ConnectedSocket() client: Socket,
    ): Promise<void> {
        const user = await this.authService.getUserFromSocket(client);
        const challengeSent: GetChallengeDto = JSON.parse(challenge.data);
        console.log('Challenge sent by user: ', user?.email);
        // Get challenged user
        const challengedUser = challengeSent.challengeInfos.find(
            (challengeInfo) => !challengeInfo.isChallenger,
        );
        // console.log('Found challenged user: ', challengedUser?.user?.email);
        const foundSocketIdByChallengedUser = this.connectedUsers.find(
            (connectedUser) => connectedUser.user.id === challengedUser.user.id,
        );
        // console.log(
        //     'Found socket id by challenged user: ',
        //     foundSocketIdByChallengedUser,
        // );
        if (foundSocketIdByChallengedUser) {
            console.log(
                'Found socket id by challenged user: ',
                foundSocketIdByChallengedUser.socketId,
            );
            this.server
                .to(foundSocketIdByChallengedUser.socketId)
                .emit('receive_challenge', challengeSent);
        }
    }
}
