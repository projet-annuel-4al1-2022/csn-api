import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';

export class CreateChallengeDto {
    @ApiProperty()
    @IsNotEmpty()
    exerciseId: number;

    @ApiProperty()
    @IsNotEmpty()
    challengerId: number;

    @ApiProperty()
    @IsNotEmpty()
    challengedId: number;
}
