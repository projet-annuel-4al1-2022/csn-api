import { ApiProperty } from '@nestjs/swagger';
import { GetUserDto } from '../../../users/DTO/get-user.dto';

export class GetMutualsRankingSortedByScoreMeanDto {
    @ApiProperty()
    readonly mutual: GetUserDto;
    @ApiProperty()
    readonly score: number;
}
