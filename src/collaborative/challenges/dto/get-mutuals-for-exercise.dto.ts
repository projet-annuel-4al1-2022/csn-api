import { GetUserDto } from '../../../users/DTO/get-user.dto';

export class GetMutualsForExerciseDto {
    public readonly user: GetUserDto;
    public readonly hasBeenAlreadyChallengedByCurrentUserOnExercise: boolean;
}
