import { Code } from '../../../code/entities/code.entity';

export class UpdateChallengeInfoDto {
    public readonly isChallenger: boolean;
    public readonly hasSubmittedExercise?: boolean;
    public readonly hasValidatedExercise?: boolean;
    public readonly timeSpentOnExercise?: number;
    public readonly executionTime?: number;
    public readonly usedMemory?: number;
    public readonly userCode?: Code;
    public readonly score?: number;
}
