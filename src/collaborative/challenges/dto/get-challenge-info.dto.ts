import { ApiProperty } from '@nestjs/swagger';
import { GetUserDto } from '../../../users/DTO/get-user.dto';
import { GetChallengeDto } from './get-challenge.dto';
import { GetCodeDto } from '../../../code/dto/get-code.dto';

export class GetChallengeInfoDto {
    @ApiProperty()
    readonly user: GetUserDto;
    @ApiProperty()
    readonly challenge: GetChallengeDto;
    @ApiProperty()
    readonly hasSubmittedExercise: boolean;
    @ApiProperty()
    readonly hasValidatedExercise: boolean;
    @ApiProperty()
    readonly timeSpentOnExercise: number;
    @ApiProperty()
    readonly executionTime: number;
    @ApiProperty()
    readonly userCode: GetCodeDto;
    @ApiProperty()
    readonly isChallenger: boolean;
    @ApiProperty()
    readonly score: number;
}
