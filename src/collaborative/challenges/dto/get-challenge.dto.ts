import { GetExerciseDto } from '../../exercises/dto/get-exercise.dto';
import { ApiProperty } from '@nestjs/swagger';
import { GetChallengeInfoDto } from './get-challenge-info.dto';

export class GetChallengeDto {
    @ApiProperty()
    id: number;
    @ApiProperty()
    exercise: GetExerciseDto;
    @ApiProperty()
    challengeInfos: GetChallengeInfoDto[];
}
