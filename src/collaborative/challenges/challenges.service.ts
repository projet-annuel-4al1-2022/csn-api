import { Injectable } from '@nestjs/common';
import { CreateChallengeDto } from './dto/create-challenge.dto';
import { UpdateChallengeInfoDto } from './dto/update-challenge-info.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { In, Repository } from 'typeorm';
import { Challenge } from './entities/challenge.entity';
import { UsersService } from '../../users/users.service';
import { ExercisesService } from '../exercises/exercises.service';
import { ChallengeInfo } from './entities/challenger-info.entity';
import { GetMutualsForExerciseDto } from './dto/get-mutuals-for-exercise.dto';
import { CodesService } from '../../code/codes.service';
import { Language } from '../../code/entities/code.entity';
import { GetMutualsRankingSortedByScoreMeanDto } from './dto/get-mutuals-ranking-challenges';

@Injectable()
export class ChallengesService {
    constructor(
        @InjectRepository(Challenge)
        private readonly challengesRepository: Repository<Challenge>,
        @InjectRepository(ChallengeInfo)
        private readonly challengeInfosRepository: Repository<ChallengeInfo>,
        private readonly usersService: UsersService,
        private readonly exercisesService: ExercisesService,
        private readonly codesService: CodesService,
    ) {}

    async create(createChallengeDto: CreateChallengeDto): Promise<Challenge> {
        // Find both users
        const challenger = await this.usersService.findById(
            createChallengeDto.challengerId,
        );
        const challenged = await this.usersService.findById(
            createChallengeDto.challengedId,
        );
        if (!challenger || !challenged) {
            throw new Error(
                `User not found with id = '${createChallengeDto.challengerId}' or '${createChallengeDto.challengedId}'`,
            );
        }
        // Find Exercice
        const exercise = await this.exercisesService.findById(
            createChallengeDto.exerciseId,
        );

        if (!exercise) {
            throw new Error(
                `Exercise not found with id = '${createChallengeDto.exerciseId}'`,
            );
        }

        // Create challenge
        const challenge = await this.challengesRepository.create({
            exercise,
        });

        const savedChallenge = await this.saveChallenge(challenge);

        // Create Challenge Infos
        const savedChallengeInfoChallenger = await this.saveChallengeInfo(
            this.challengeInfosRepository.create({
                isChallenger: true,
                challenge: savedChallenge,
                user: challenger,
            }),
        );

        const savedChallengeInfoChallenged = await this.saveChallengeInfo(
            this.challengeInfosRepository.create({
                isChallenger: false,
                challenge: savedChallenge,
                user: challenged,
            }),
        );

        challenge.challengeInfo = [
            savedChallengeInfoChallenger,
            savedChallengeInfoChallenged,
        ];

        return this.saveChallenge(challenge);
    }

    findAll() {
        return this.challengesRepository.find({
            relations: [
                'exercise',
                'challengeInfo',
                'challengeInfo.user',
                'challengeInfo.userCode',
            ],
        });
    }

    async findById(challengeId: number): Promise<Challenge> {
        return this.challengesRepository.findOneOrFail(challengeId, {
            relations: [
                'exercise',
                'exercise.validatingTests',
                'challengeInfo',
                'challengeInfo.challenge',
                'challengeInfo.user',
                'challengeInfo.userCode',
            ],
        });
    }

    async findChallengerInfoByChallengeAndUser(
        challengeId: number,
        userId: number,
    ) {
        return this.challengeInfosRepository.findOneOrFail({
            where: {
                challenge: {
                    id: challengeId,
                },
                user: {
                    id: userId,
                },
            },
            relations: ['challenge', 'challenge.exercise', 'user', 'userCode'],
        });
    }

    async updateChallengeInfos(
        challengeId: number,
        userId: number,
        updateChallengeDto: UpdateChallengeInfoDto,
    ): Promise<ChallengeInfo> {
        const challengeInfo = await this.findChallengerInfoByChallengeAndUser(
            challengeId,
            userId,
        );

        challengeInfo.hasSubmittedExercise =
            updateChallengeDto.hasSubmittedExercise ??
            challengeInfo.hasSubmittedExercise;
        challengeInfo.hasValidatedExercise =
            updateChallengeDto.hasValidatedExercise ??
            challengeInfo.hasValidatedExercise;
        challengeInfo.timeSpentOnExercise =
            updateChallengeDto.timeSpentOnExercise ??
            challengeInfo.timeSpentOnExercise;
        challengeInfo.executionTime =
            updateChallengeDto.executionTime ?? challengeInfo.executionTime;
        challengeInfo.usedMemory =
            updateChallengeDto.usedMemory ?? challengeInfo.usedMemory;
        challengeInfo.userCode =
            updateChallengeDto.userCode ?? challengeInfo.userCode;
        challengeInfo.score = updateChallengeDto.score ?? challengeInfo.score;

        return this.saveChallengeInfo(challengeInfo);
    }

    remove(challengeId: number) {
        return this.challengesRepository.delete(challengeId);
    }

    async saveChallenge(challenge: Challenge): Promise<Challenge> {
        const exerciseSaved = await this.challengesRepository.save(challenge);
        return this.findById(exerciseSaved.id);
    }

    async saveChallengeInfo(challengeInfo: ChallengeInfo) {
        const saved = await this.challengeInfosRepository.save(challengeInfo);
        return this.findChallengerInfoByChallengeAndUser(
            saved.challenge.id,
            saved.user.id,
        );
    }

    async findChallengerInfo(challengeId: number) {
        const challenge = await this.findById(challengeId);
        return challenge.challengeInfo.find(
            (challengeInfo) => challengeInfo.isChallenger,
        );
    }

    async findChallengedInfo(challengeId: number) {
        const challenge = await this.findById(challengeId);
        return challenge.challengeInfo.find(
            (challengeInfo) => !challengeInfo.isChallenger,
        );
    }

    findChallengeInfo(challengeId: number) {
        return this.challengeInfosRepository.findOneOrFail({
            where: {
                challenge: {
                    id: challengeId,
                },
            },
        });
    }

    async getOnGoingChallenges(id: number): Promise<Challenge[]> {
        const userChallengeInfos = await this.challengeInfosRepository.find({
            where: {
                user: {
                    id: id,
                },
                hasSubmittedExercise: false,
            },
            relations: ['challenge'],
        });
        return this.challengesRepository.find({
            where: {
                id: In(
                    userChallengeInfos.map(
                        (challengeInfo) => challengeInfo.challenge.id,
                    ),
                ),
            },
            relations: ['exercise', 'challengeInfo', 'challengeInfo.user'],
        });
    }

    async updateTimePassed(
        challengeId: number,
        userAndTimePassed: {
            userId: number;
            timePassedOnExerciseInSeconds: number;
        },
    ) {
        await this.challengeInfosRepository.update(
            {
                challenge: {
                    id: challengeId,
                },
                user: {
                    id: userAndTimePassed.userId,
                },
            },
            {
                timeSpentOnExercise:
                    userAndTimePassed.timePassedOnExerciseInSeconds,
            },
        );
        return this.findById(challengeId);
    }

    async userMutualsForExercise(
        exerciseId: number,
        userId: number,
    ): Promise<GetMutualsForExerciseDto[]> {
        const mutuals = await this.usersService.getMutuals(userId);
        // Return mutuals with hasAlreadyChallengedUser data
        const foundChallengeByExerciseIdAndChallengerId =
            await this.findChallengeInfoByExerciseIdAndChallengerId(
                exerciseId,
                userId,
            );

        return mutuals.map((mutual) => {
            const challengeWhereMutualIsChallengedByUser =
                foundChallengeByExerciseIdAndChallengerId.find((challenge) => {
                    return challenge.challengeInfo.find((challengeInfo) => {
                        return (
                            challengeInfo.user.id === mutual.id &&
                            !challengeInfo.isChallenger
                        );
                    });
                });
            return {
                user: mutual.toDto(),
                hasBeenAlreadyChallengedByCurrentUserOnExercise:
                    challengeWhereMutualIsChallengedByUser != null,
            };
        });
    }

    private async findChallengeInfoByExerciseIdAndChallengerId(
        exerciseId: number,
        userId: number,
    ) {
        const foundChallenges = await this.challengesRepository.find({
            where: {
                exercise: {
                    id: exerciseId,
                },
            },
            relations: ['challengeInfo', 'challengeInfo.user', 'exercise'],
        });
        return foundChallenges
            .map((challenge) => {
                if (
                    challenge.challengeInfo.find(
                        (challengeInfo) =>
                            challengeInfo.user.id == userId &&
                            challengeInfo.isChallenger,
                    )
                ) {
                    return challenge;
                }
            })
            .filter((challengeInfo) => challengeInfo != null);
    }

    async uploadCode(
        challengeId: number,
        userAndCode: { userId: number; code: string; language: string },
    ): Promise<Challenge> {
        const challenge = await this.findById(challengeId);
        if (challenge == null) {
            throw new Error(`Challenge not found with id = ${challengeId}`);
        }
        if (challenge.exercise.language !== userAndCode.language) {
            throw new Error(
                `Language of challenge and code don't match : ${challenge.exercise.language} != ${userAndCode.language}`,
            );
        }
        const challengeInfo = challenge.challengeInfo.find(
            (challengeInfo) => challengeInfo.user.id == userAndCode.userId,
        );
        if (challengeInfo == null) {
            throw new Error('Challenge info not found');
        }

        // Create code entity
        challengeInfo.userCode = await this.codesService.create({
            code: userAndCode.code,
            language: userAndCode.language,
            title: challenge.exercise.name + ' - ' + new Date().toISOString(),
        });

        // Save challenge info
        await this.saveChallengeInfo(challengeInfo);

        return challenge;
    }

    async submitExercise(
        challengeId: number,
        userId: number,
        codeContent: string,
        codeLanguage: string,
        numberOfValidatedTests: number,
    ) {
        const challenge = await this.findById(challengeId);
        const challengeInfo = await this.findChallengerInfoByChallengeAndUser(
            challengeId,
            userId,
        );
        if (challengeInfo == null) {
            throw new Error(
                'Challenge info not found for user ' +
                    userId +
                    ' and challenge ' +
                    challengeId,
            );
        }
        if (!challengeInfo.userCode) {
            challengeInfo.userCode = await this.codesService.create({
                code: codeContent,
                language: Object.keys(Language)[codeLanguage],
                title:
                    challenge.exercise.name + ' - ' + new Date().toISOString(),
            });
        }

        challengeInfo.hasValidatedExercise =
            numberOfValidatedTests == challenge.exercise.validatingTests.length;
        challengeInfo.hasSubmittedExercise = true;

        if (numberOfValidatedTests >= 1) {
            challengeInfo.score += challengeInfo.hasValidatedExercise ? 100 : 0;

            challengeInfo.score =
                100 *
                (numberOfValidatedTests /
                    challenge.exercise.validatingTests.length);

            if (
                numberOfValidatedTests ==
                challenge.exercise.validatingTests.length
            ) {
                const timeSpentOnExerciseScore =
                    challenge.exercise.averageTime <
                    challengeInfo.timeSpentOnExercise
                        ? 0
                        : 60;
                challengeInfo.score += timeSpentOnExerciseScore;
                console.log(
                    'timeSpentOnExerciseScore',
                    timeSpentOnExerciseScore,
                );
            }
            const memoryScore =
                challenge.exercise.memoryLimit < challengeInfo.usedMemory
                    ? 0
                    : 50;
            console.log('memoryScore', memoryScore);

            const executionTimeScore =
                challengeInfo.executionTime > 5
                    ? 0
                    : 5 - challengeInfo.executionTime * 10;
            console.log('executionTimeScore', executionTimeScore);

            challengeInfo.score += memoryScore + executionTimeScore;
        } else {
            challengeInfo.score = 0;
        }

        challengeInfo.score = Math.round(challengeInfo.score);

        return this.saveChallengeInfo(challengeInfo);
    }

    //Récupère les challenges de l'utilisateur lorsqu'il est le challenger.
    async findUserChallengesWhenChallenger(userId : number){
        const challenges = await this.challengesRepository.find({
            relations: ['challengeInfo', 'challengeInfo.user', 'exercise'],
        });

        return challenges
            .map((challenge) => {
                const challengeInfo = challenge.challengeInfo.find(
                    (challengeInfo) =>
                        challengeInfo.user.id == userId &&
                        challengeInfo.hasSubmittedExercise == true &&
                        challengeInfo.isChallenger == true,
                );
                if (challengeInfo == null) {
                    return null;
                }
                return {
                    challenge: challenge.toDto(),
                };
            })
            .filter((challenge) => challenge != null);
    }

    //Récupère les challenges de l'utilisateur lorsqu'il est l'utilisateur défié.
    async findUserChallengesWhenChallenged(userId : number){
        const challenges = await this.challengesRepository.find({
            relations: ['challengeInfo', 'challengeInfo.user', 'exercise'],
        });

        return challenges
            .map((challenge) => {
                const challengeInfo = challenge.challengeInfo.find(

                    (challengeInfo) => challengeInfo.user.id == userId && challengeInfo.hasSubmittedExercise == true && challengeInfo.isChallenger == false,
                );
                if (challengeInfo == null) {
                    return null;
                }
                return {
                    challenge: challenge.toDto()
                };
            })
            .filter((challenge) => challenge != null);
    }

    async mutualsRankingSortedByScoreMean(
        userId: number,
    ): Promise<GetMutualsRankingSortedByScoreMeanDto[]> {
        const mutuals = await this.usersService.getMutuals(userId);
        mutuals.push(await this.usersService.findById(userId));
        const mutualsChallenges = new Map();
        for (const mutual of mutuals) {
            const challenges = mutual.challengeInfos;
            const filteredChallenges = challenges.filter(
                (challenge) => challenge.hasSubmittedExercise == true,
            );
            mutualsChallenges.set(mutual, filteredChallenges);
        }

        const sortedMutualsMeanScore: GetMutualsRankingSortedByScoreMeanDto[] =
            [];

        for (const [mutual, challenges] of mutualsChallenges) {
            let totalScore = 0;
            for (const challenge of challenges) {
                totalScore += challenge.score;
            }
            sortedMutualsMeanScore.push({
                mutual: mutual.toDto(),
                score: Number(totalScore / challenges.length),
            });
        }

        return sortedMutualsMeanScore.sort((a, b) => b.score - a.score);
    }
}
