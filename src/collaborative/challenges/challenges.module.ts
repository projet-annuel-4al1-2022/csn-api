import { forwardRef, Module } from '@nestjs/common';
import { ChallengesService } from './challenges.service';
import { ChallengesController } from './challenges.controller';
import { UsersModule } from '../../users/users.module';
import { ExercisesModule } from '../exercises/exercises.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Challenge } from './entities/challenge.entity';
import { ChallengeInfo } from './entities/challenger-info.entity';
import { CodesModule } from '../../code/codes.module';

@Module({
    controllers: [ChallengesController],
    providers: [ChallengesService],
    exports: [ChallengesService],
    imports: [
        TypeOrmModule.forFeature([Challenge]),
        TypeOrmModule.forFeature([ChallengeInfo]),
        forwardRef(() => UsersModule),
        forwardRef(() => ExercisesModule),
        forwardRef(() => CodesModule),
    ],
})
export class ChallengesModule {}
