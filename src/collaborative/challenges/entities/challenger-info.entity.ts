import {
    Column,
    Entity,
    Index,
    JoinColumn,
    ManyToOne,
    OneToOne,
} from 'typeorm';
import { User } from '../../../users/entities/user.entity';
import { Challenge } from './challenge.entity';
import { Code } from '../../../code/entities/code.entity';
import { GetChallengeInfoDto } from '../dto/get-challenge-info.dto';

// Primary key is determined by user & challenge
@Entity({ name: 'challenge_info' })
@Index(['user', 'challenge'], { unique: true })
export class ChallengeInfo {
    @ManyToOne(() => User, (user) => user.challengeInfos, {
        primary: true,
    })
    @JoinColumn({ name: 'user_id' })
    public user: User;

    @ManyToOne(() => Challenge, (challenge) => challenge.challengeInfo, {
        primary: true,
    })
    @JoinColumn({ name: 'challenge_id' })
    public challenge: Challenge;

    // Has submitted exercise
    @Column({ name: 'has_submitted_exercise', default: false })
    public hasSubmittedExercise: boolean;

    // Has validated exercise
    @Column({ name: 'has_validated_exercise', default: false })
    public hasValidatedExercise: boolean;

    // Time spent on exercise
    @Column({ name: 'time_spent_on_exercise', default: 0 })
    public timeSpentOnExercise: number;

    // Execution time
    @Column({
        name: 'execution_time',
        default: 0,
        type: 'float',
    })
    public executionTime: number; // In seconds

    // Used memory
    @Column({
        name: 'used_memory',
        default: null,
        type: 'float',
        scale: 2,
    })
    public usedMemory: number;

    // User code
    @OneToOne(() => Code, (code) => code.challengeInfo, { nullable: true })
    @JoinColumn({ name: 'code_id' })
    public userCode?: Code;

    // Is the challenger (the one who threw the challenge to the challenged user)
    @Column({ name: 'is_challenger', default: false })
    public isChallenger: boolean;

    // Score
    @Column({ name: 'score', default: 0 })
    public score: number;

    toDto(): GetChallengeInfoDto {
        return {
            user: this.user?.toDto(),
            challenge: this.challenge?.toDto(),
            hasSubmittedExercise: this.hasSubmittedExercise,
            hasValidatedExercise: this.hasValidatedExercise,
            timeSpentOnExercise: this.timeSpentOnExercise,
            executionTime: this.executionTime,
            userCode: this.userCode?.toDto(),
            isChallenger: this.isChallenger,
            score: this.score,
        };
    }
}
