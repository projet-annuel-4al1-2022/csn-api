import {
    CreateDateColumn,
    DeleteDateColumn,
    Entity,
    JoinColumn,
    ManyToOne,
    OneToMany,
    PrimaryGeneratedColumn,
    UpdateDateColumn,
} from 'typeorm';
import { Exercise } from '../../exercises/entities/exercice.entity';
import { GetChallengeDto } from '../dto/get-challenge.dto';
import { ChallengeInfo } from './challenger-info.entity';

@Entity({ name: 'challenges' })
export class Challenge {
    @PrimaryGeneratedColumn('increment') id: number;

    @ManyToOne(() => Exercise, (exercise) => exercise.challenges)
    @JoinColumn({ name: 'exercise_id' })
    exercise: Exercise;

    @OneToMany(() => ChallengeInfo, (challengeInfo) => challengeInfo.challenge)
    public challengeInfo: ChallengeInfo[];

    @CreateDateColumn({
        type: 'timestamptz',
        name: 'created_at',
    })
    public createdAt: Date;

    @UpdateDateColumn({
        type: 'timestamptz',
        name: 'updated_at',
    })
    public updatedAt: Date;

    @DeleteDateColumn({
        type: 'timestamptz',
        name: 'deleted_at',
        nullable: true,
    })
    public deletedAt: Date;

    toDto(): GetChallengeDto {
        return {
            id: this.id,
            exercise: this.exercise?.toDto(),
            challengeInfos: this.challengeInfo?.map((challengeInfo) =>
                challengeInfo.toDto(),
            ),
        };
    }
}
