import {
    Body,
    Controller,
    Delete,
    Get,
    Param,
    Patch,
    Post,
} from '@nestjs/common';
import { ChallengesService } from './challenges.service';
import { CreateChallengeDto } from './dto/create-challenge.dto';
import { UpdateChallengeInfoDto } from './dto/update-challenge-info.dto';
import { GetChallengeDto } from './dto/get-challenge.dto';
import { GetChallengeInfoDto } from './dto/get-challenge-info.dto';
import { GetMutualsForExerciseDto } from './dto/get-mutuals-for-exercise.dto';
import { GetMutualsRankingSortedByScoreMeanDto } from './dto/get-mutuals-ranking-challenges';

@Controller('challenges')
export class ChallengesController {
    constructor(private readonly challengesService: ChallengesService) {}

    @Post()
    async create(
        @Body() createChallengeDto: CreateChallengeDto,
    ): Promise<GetChallengeDto> {
        return (
            await this.challengesService.create(createChallengeDto)
        ).toDto();
    }

    @Get()
    async findAll(): Promise<GetChallengeDto[]> {
        return (await this.challengesService.findAll()).map((challenge) =>
            challenge.toDto(),
        );
    }

    @Get(':challengeId')
    async findOne(
        @Param('challengeId') challengeId: number,
    ): Promise<GetChallengeDto> {
        return (await this.challengesService.findById(challengeId)).toDto();
    }

    @Delete(':challengeId')
    remove(@Param('challengeId') challengeId: number) {
        return this.challengesService.remove(challengeId);
    }

    @Get(':challengeId/info')
    async findChallengeInfo(
        @Param('challengeId') challengeId: number,
    ): Promise<GetChallengeInfoDto> {
        return (
            await this.challengesService.findChallengeInfo(challengeId)
        ).toDto();
    }

    @Get(':challengeId/challenger-info')
    async findChallengerInfo(
        @Param('challengeId') challengeId: number,
    ): Promise<GetChallengeInfoDto> {
        return (
            await this.challengesService.findChallengerInfo(challengeId)
        ).toDto();
    }

    @Get(':challengeId/challenged-info')
    async findChallengedInfo(@Param('challengeId') challengeId: number) {
        return (
            await this.challengesService.findChallengedInfo(challengeId)
        ).toDto();
    }

    @Patch(':challengeId/user/:userId')
    updateChallengeInfo(
        @Param('challengeId') challengeId: number,
        @Param('userId') userId: number,
        @Body() updateChallengeDto: UpdateChallengeInfoDto,
    ) {
        return this.challengesService.updateChallengeInfos(
            challengeId,
            userId,
            updateChallengeDto,
        );
    }

    // Get on going user challenges
    @Get('on-going/:id')
    async getOnGoingChallenges(@Param('id') id: number) {
        return (await this.challengesService.getOnGoingChallenges(id)).map(
            (challenge) => challenge.toDto(),
        );
    }

    // Update time passed on challenge exercise
    @Patch(':challengeId/time-passed')
    async updateTimePassed(
        @Param('challengeId') challengeId: number,
        @Body()
        userAndTimePassed: {
            userId: number;
            timePassedOnExerciseInSeconds: number;
        },
    ): Promise<GetChallengeDto> {
        return (
            await this.challengesService.updateTimePassed(
                challengeId,
                userAndTimePassed,
            )
        ).toDto();
    }

    @Get('/with-exercise/:exerciseId/mutuals/:userId')
    async userMutualsForExercise(
        @Param('exerciseId') exerciseId: number,
        @Param('userId') userId: number,
    ): Promise<GetMutualsForExerciseDto[]> {
        return await this.challengesService.userMutualsForExercise(
            exerciseId,
            userId,
        );
    }

    // Upload code
    // challenges/${challengeId}/upload
    @Post(':challengeId/upload')
    async uploadCode(
        @Param('challengeId') challengeId: number,
        @Body()
        userAndCode: {
            userId: number;
            code: string;
            language: string;
        },
    ): Promise<GetChallengeDto> {
        return (
            await this.challengesService.uploadCode(challengeId, userAndCode)
        ).toDto();
    }

    @Post(':challengeId/submit')
    async submitExercise(
        @Param('challengeId') challengeId: number,
        @Body()
        userAndCode: {
            codeContent: string;
            codeLanguage: string;
            numberOfValidatedTests: number;
            userId: number;
        },
    ): Promise<GetChallengeInfoDto> {
        console.log('Challenge infos passed to submitExercise', userAndCode);
        return (
            await this.challengesService.submitExercise(
                challengeId,
                userAndCode.userId,
                userAndCode.codeContent,
                userAndCode.codeLanguage,
                userAndCode.numberOfValidatedTests,
            )
        ).toDto();
    }

    //Récupère les challenges lorsque l'utilisateur est le challenger.
    @Get('/userChallenger/:userId')
    async getUserChallengesWhenChallenger(@Param('userId') userId: number) {
        return (await this.challengesService.findUserChallengesWhenChallenger(userId)).map(
            (challenge) => challenge,
        );
    }

    //Récupère les challenges lorsque l'utilisateur est défiée.
    @Get('/userChallenged/:userId')
    async getUserChallengesWhenChallenged(@Param('userId') userId: number) {
        return (await this.challengesService.findUserChallengesWhenChallenged(userId)).map(
            (challenge) => challenge,
        );
    }

    @Get('/mutuals/ranking/:userId')
    async mutualsRankingSortedByScoreMean(
        @Param('userId') userId: number,
    ): Promise<GetMutualsRankingSortedByScoreMeanDto[]> {
        return await this.challengesService.mutualsRankingSortedByScoreMean(
            userId,
        );
    }
}
