import { Injectable } from '@nestjs/common';
import { CreateValidatingTestDto } from './dto/create-validating-test.dto';
import { UpdateValidatingTestDto } from './dto/update-validating-test.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { ValidatingTest } from './entities/validating-test.entity';
import { Repository } from 'typeorm';
import { ExercisesService } from '../exercises/exercises.service';

@Injectable()
export class ValidatingTestService {
    constructor(
        @InjectRepository(ValidatingTest)
        private readonly validatingTestRepository: Repository<ValidatingTest>,
        private readonly exercisesService: ExercisesService,
    ) {}

    async create(createValidatingTestDto: CreateValidatingTestDto) {
        const exercise = await this.exercisesService.findById(
            createValidatingTestDto.exerciseId,
        );

        if (!exercise) {
            throw new Error(
                'Exercise not found with id : ' +
                    createValidatingTestDto.exerciseId,
            );
        }

        const validatingTest = await this.validatingTestRepository.create({
            exercise,
            name: createValidatingTestDto.name,
            description: createValidatingTestDto.description,
            codeToExecute: createValidatingTestDto.codeToExecute,
            input: createValidatingTestDto.inputParameters,
            expectedOutput: createValidatingTestDto.expectedOutput,
            difficulty: createValidatingTestDto.difficulty,
        });

        return this.save(validatingTest);
    }

    findAll() {
        return this.validatingTestRepository.find();
    }

    findById(id: number) {
        return this.validatingTestRepository.findOne(id);
    }

    update(id: number, updateValidatingTestDto: UpdateValidatingTestDto) {
        return this.validatingTestRepository.update(
            id,
            updateValidatingTestDto,
        );
    }

    remove(id: number) {
        return this.validatingTestRepository.delete(id);
    }

    async save(validatingTest: ValidatingTest) {
        const saved = await this.validatingTestRepository.save(validatingTest);
        return this.findById(saved.id);
    }
}
