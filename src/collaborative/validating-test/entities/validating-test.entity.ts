import {
    Column,
    Entity,
    Index,
    JoinColumn,
    ManyToOne,
    PrimaryGeneratedColumn,
} from 'typeorm';
import { Difficulty, Exercise } from '../../exercises/entities/exercice.entity';
import { GetValidatingTestDto } from '../dto/get-validating-test.dto';

@Entity({ name: 'validating_tests' })
@Index(['name', 'exercise'], { unique: true })
export class ValidatingTest {
    @PrimaryGeneratedColumn('increment') id: number;
    @Column({ name: 'name' })
    public name: string;

    @Column({ name: 'description', default: '' })
    public description: string;

    @Column({ name: 'code_to_execute' })
    public codeToExecute: string;

    @Column({ name: 'input', default: '' })
    public input: string;

    @Column({ name: 'expected_output', nullable: true })
    public expectedOutput?: string;

    @Column({
        name: 'difficulty',
        enum: Difficulty,
    })
    public difficulty: Difficulty;

    @ManyToOne(() => Exercise, (exercise) => exercise.validatingTests)
    @JoinColumn({ name: 'exercise_id' })
    public exercise: Exercise;

    toDto(): GetValidatingTestDto {
        return {
            id: this.id,
            name: this.name,
            description: this.description,
            codeToExecute: this.codeToExecute,
            input: this.input,
            expectedOutput: this.expectedOutput,
            difficulty: this.difficulty,
            exercise: this.exercise?.toDto(),
        };
    }
}
