import { forwardRef, Module } from '@nestjs/common';
import { ValidatingTestService } from './validating-test.service';
import { ValidatingTestController } from './validating-test.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ValidatingTest } from './entities/validating-test.entity';
import { ExercisesModule } from '../exercises/exercises.module';

@Module({
    controllers: [ValidatingTestController],
    providers: [ValidatingTestService],
    exports: [ValidatingTestService],
    imports: [
        TypeOrmModule.forFeature([ValidatingTest]),
        forwardRef(() => ExercisesModule),
    ],
})
export class ValidatingTestModule {}
