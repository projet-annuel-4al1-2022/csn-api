import { Difficulty } from '../../exercises/entities/exercice.entity';
import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';
import { GetExerciseDto } from '../../exercises/dto/get-exercise.dto';

export class GetValidatingTestDto {
    @ApiProperty()
    id: number;

    @ApiProperty()
    name: string;

    @ApiProperty()
    description: string;

    @ApiProperty()
    codeToExecute: string;

    @ApiProperty()
    input: string;

    @ApiProperty()
    expectedOutput: string;

    @ApiProperty()
    difficulty: Difficulty;

    @ApiProperty()
    @IsNotEmpty()
    exercise: GetExerciseDto;
}
