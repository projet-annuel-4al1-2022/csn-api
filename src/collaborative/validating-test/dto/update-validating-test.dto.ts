import { PartialType } from '@nestjs/swagger';
import { CreateValidatingTestDto } from './create-validating-test.dto';

export class UpdateValidatingTestDto extends PartialType(
    CreateValidatingTestDto,
) {}
