import { Difficulty } from '../../exercises/entities/exercice.entity';
import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, ValidateIf } from 'class-validator';

export class CreateValidatingTestDto {
    @ApiProperty()
    @IsNotEmpty()
    name: string;

    @ApiProperty()
    description: string;

    @ApiProperty()
    @IsNotEmpty()
    codeToExecute: string;

    @ApiProperty()
    inputParameters: string;

    @ApiProperty()
    @ValidateIf((o) => (!o.returnValue && !o.returnType) || o.expectedOutput)
    expectedOutput: string;

    @ApiProperty()
    @IsNotEmpty()
    difficulty: Difficulty;

    @ApiProperty()
    @IsNotEmpty()
    exerciseId: number;
}
