import {
    Body,
    Controller,
    Delete,
    Get,
    Param,
    Patch,
    Post,
} from '@nestjs/common';
import { ValidatingTestService } from './validating-test.service';
import { CreateValidatingTestDto } from './dto/create-validating-test.dto';
import { UpdateValidatingTestDto } from './dto/update-validating-test.dto';

@Controller('validating-test')
export class ValidatingTestController {
    constructor(
        private readonly validatingTestService: ValidatingTestService,
    ) {}

    @Post()
    create(@Body() createValidatingTestDto: CreateValidatingTestDto) {
        return this.validatingTestService.create(createValidatingTestDto);
    }

    @Get()
    findAll() {
        return this.validatingTestService.findAll();
    }

    @Get(':id')
    findOne(@Param('id') id: number) {
        return this.validatingTestService.findById(id);
    }

    @Patch(':id')
    update(
        @Param('id') id: number,
        @Body() updateValidatingTestDto: UpdateValidatingTestDto,
    ) {
        return this.validatingTestService.update(id, updateValidatingTestDto);
    }

    @Delete(':id')
    remove(@Param('id') id: number) {
        return this.validatingTestService.remove(id);
    }
}
