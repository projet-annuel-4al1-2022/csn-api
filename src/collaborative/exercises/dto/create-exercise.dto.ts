import { Language } from '../../../code/entities/code.entity';
import { Difficulty } from '../entities/exercice.entity';
import { ApiProperty } from '@nestjs/swagger';
import { IsEnum, IsNotEmpty } from 'class-validator';
import { CreateValidatingTestDto } from '../../validating-test/dto/create-validating-test.dto';

export class CreateExerciseDto {
    @ApiProperty()
    @IsNotEmpty()
    readonly name: string;
    @ApiProperty()
    @IsNotEmpty()
    readonly instructions: string;
    @ApiProperty()
    @IsNotEmpty()
    @IsEnum(Difficulty)
    readonly difficulty: Difficulty;
    @ApiProperty()
    @IsNotEmpty()
    @IsEnum(Language)
    readonly language: Language;

    @ApiProperty()
    @IsNotEmpty()
    public defaultCode: string;

    @ApiProperty()
    @IsNotEmpty()
    public validatingTests?: CreateValidatingTestDto[] = [];

    @ApiProperty()
    @IsNotEmpty()
    public memoryLimit = 1024;
}
