import { Language } from '../../../code/entities/code.entity';
import { Difficulty } from '../entities/exercice.entity';
import { ApiProperty } from '@nestjs/swagger';
import { IsEnum } from 'class-validator';

export class UpdateExerciseDto {
    @ApiProperty()
    readonly name?: string;
    @ApiProperty()
    readonly instructions?: string;
    @ApiProperty()
    @IsEnum(Difficulty)
    readonly difficulty?: Difficulty;
    @ApiProperty()
    @IsEnum(Language)
    readonly language?: Language;
    @ApiProperty()
    readonly memoryLimit?: number;
}
