import { Language } from '../../../code/entities/code.entity';
import { Difficulty } from '../entities/exercice.entity';
import { ApiProperty } from '@nestjs/swagger';
import { GetValidatingTestDto } from '../../validating-test/dto/get-validating-test.dto';

export class GetExerciseDto {
    @ApiProperty()
    readonly id: number;
    @ApiProperty()
    readonly name: string;
    @ApiProperty()
    readonly instructions: string;
    @ApiProperty()
    readonly difficulty: Difficulty;
    @ApiProperty()
    readonly language: Language;
    @ApiProperty()
    readonly defaultCode: string;
    @ApiProperty()
    readonly validatingTests: GetValidatingTestDto[];
    @ApiProperty()
    readonly memoryLimit: number;
}
