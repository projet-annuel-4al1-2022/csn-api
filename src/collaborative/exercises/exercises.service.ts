import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { config } from 'dotenv';
import { Difficulty, Exercise } from './entities/exercice.entity';
import { CreateExerciseDto } from './dto/create-exercise.dto';
import { Language } from '../../code/entities/code.entity';
import { UpdateExerciseDto } from './dto/update-exercise.dto';

config();

@Injectable()
export class ExercisesService {
    constructor(
        @InjectRepository(Exercise)
        private readonly exercisesRepository: Repository<Exercise>,
    ) {}

    async create(createExerciseDto: CreateExerciseDto): Promise<Exercise> {
        const saved = await this.exercisesRepository.save({
            ...createExerciseDto,
        });
        return this.exercisesRepository.findOne(saved.id);
    }

    async update(
        exerciseId: number,
        updateExerciseDto: UpdateExerciseDto,
    ): Promise<Exercise> {
        const exercise = await this.findById(exerciseId);
        const exerciseUpdated = await this.exercisesRepository.save({
            ...exercise,
            ...updateExerciseDto,
        });
        return this.findById(exerciseUpdated.id);
    }

    async findAll(): Promise<Exercise[]> {
        return this.exercisesRepository.find({
            relations: ['validatingTests'],
        });
    }

    async findById(exerciseId: number): Promise<Exercise> {
        return this.exercisesRepository.findOneOrFail({
            where: { id: exerciseId },
            relations: ['validatingTests'],
        });
    }

    async findByLanguage(language: Language): Promise<Exercise> {
        return this.exercisesRepository
            .createQueryBuilder('exercise')
            .where('exercise.language = :language', { language: language })
            .getOneOrFail();
    }

    async findByDifficulty(difficulty: Difficulty): Promise<Exercise> {
        return this.exercisesRepository
            .createQueryBuilder('exercise')
            .where('exercise.difficulty = :difficulty', {
                difficulty: difficulty,
            })
            .getOneOrFail();
    }

    async save(exercise: Exercise): Promise<Exercise> {
        const exerciseSaved = await this.exercisesRepository.save(exercise);
        return this.findById(exerciseSaved.id);
    }

    async delete(exerciseId: number): Promise<void> {
        await this.exercisesRepository.delete(exerciseId);
    }

    async findByNameAndLanguageAndDifficulty(
        name: string,
        PYTHON: Language,
        NOOB: Difficulty,
    ) {
        return this.exercisesRepository
            .createQueryBuilder('exercise')
            .where('exercise.name = :name', { name: name })
            .andWhere('exercise.language = :language', { language: PYTHON })
            .andWhere('exercise.difficulty = :difficulty', { difficulty: NOOB })
            .getOne();
    }
}
