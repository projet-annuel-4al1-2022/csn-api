import {
    Column,
    CreateDateColumn,
    DeleteDateColumn,
    Entity,
    Index,
    OneToMany,
    PrimaryGeneratedColumn,
    UpdateDateColumn,
} from 'typeorm';
import { Language } from '../../../code/entities/code.entity';
import { GetExerciseDto } from '../dto/get-exercise.dto';
import { ValidatingTest } from '../../validating-test/entities/validating-test.entity';
import { Challenge } from '../../challenges/entities/challenge.entity';

export enum Difficulty {
    NOOB = 'NOOB',
    EASY = 'EASY',
    MEDIUM = 'MEDIUM',
    HARD = 'HARD',
    EXPERT = 'EXPERT',
}

@Entity({ name: 'exercises' })
@Index(['name', 'difficulty', 'language'], { unique: true })
export class Exercise {
    @PrimaryGeneratedColumn('increment') id: number;
    @Column({ name: 'name' })
    public name: string;

    @Column({ name: 'instructions', default: '' })
    public instructions: string;

    @Column({
        name: 'difficulty',
        enum: Difficulty,
    })
    public difficulty: Difficulty;

    @Column({ name: 'language', enum: Language })
    public language: Language;

    @Column({ name: 'default_code', default: '' })
    public defaultCode: string;

    @OneToMany(
        () => ValidatingTest,
        (validatingTest) => validatingTest.exercise,
        { eager: true },
    )
    public validatingTests: ValidatingTest[];

    @OneToMany(() => Challenge, (challenge) => challenge.exercise)
    public challenges: Challenge[];

    // Memory limit for the code to execute
    @Column({ name: 'memory_limit', default: '1024' })
    public memoryLimit: number;

    @Column({ name: 'average_time', default: 3600 / 2 })
    public averageTime: number;

    @CreateDateColumn({
        type: 'timestamptz',
        name: 'created_at',
    })
    public createdAt: Date;

    @UpdateDateColumn({
        type: 'timestamptz',
        name: 'updated_at',
    })
    public updatedAt: Date;

    @DeleteDateColumn({
        type: 'timestamptz',
        name: 'deleted_at',
        nullable: true,
    })
    public deletedAt: Date;

    toDto(): GetExerciseDto {
        return {
            id: this.id,
            name: this.name,
            instructions: this.instructions,
            difficulty: this.difficulty,
            language: this.language,
            defaultCode: this.defaultCode,
            validatingTests: this.validatingTests?.map((validatingTest) =>
                validatingTest.toDto(),
            ),
            memoryLimit: this.memoryLimit,
        };
    }
}
