import { Module } from '@nestjs/common';
import { ExercisesController } from './exercises.controller';
import { ExercisesService } from './exercises.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Exercise } from './entities/exercice.entity';

@Module({
    controllers: [ExercisesController],
    providers: [ExercisesService],
    exports: [ExercisesService],
    imports: [TypeOrmModule.forFeature([Exercise])],
})
export class ExercisesModule {}
