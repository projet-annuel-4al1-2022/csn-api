import {
    Body,
    Controller,
    Delete,
    Get,
    Param,
    Patch,
    Post,
} from '@nestjs/common';
import { GetExerciseDto } from './dto/get-exercise.dto';
import { ExercisesService } from './exercises.service';
import { Language } from '../../code/entities/code.entity';
import { Difficulty } from './entities/exercice.entity';
import { CreateExerciseDto } from './dto/create-exercise.dto';
import { UpdateExerciseDto } from './dto/update-exercise.dto';

@Controller('exercises')
export class ExercisesController {
    constructor(private readonly exercisesService: ExercisesService) {}

    @Post()
    async create(
        @Body() createExerciseDto: CreateExerciseDto,
    ): Promise<GetExerciseDto> {
        return (await this.exercisesService.create(createExerciseDto)).toDto();
    }

    @Get()
    async findAll(): Promise<GetExerciseDto[]> {
        return (await this.exercisesService.findAll()).map((exercise) =>
            exercise.toDto(),
        );
    }

    @Get('/:exerciseId')
    async findById(
        @Param('exerciseId') exerciseId: number,
    ): Promise<GetExerciseDto> {
        return (await this.exercisesService.findById(exerciseId)).toDto();
    }

    @Get('/language/:language')
    async findByLanguage(
        @Param('language') language: Language,
    ): Promise<GetExerciseDto> {
        return (await this.exercisesService.findByLanguage(language)).toDto();
    }

    @Get('/difficulty/:difficulty')
    async findByDifficulty(
        @Param('difficulty') difficulty: Difficulty,
    ): Promise<GetExerciseDto> {
        return (
            await this.exercisesService.findByDifficulty(difficulty)
        ).toDto();
    }

    @Patch('/:exerciseId')
    async update(
        @Param('exerciseId') exerciseId: number,
        @Body() updateExerciseDto: UpdateExerciseDto,
    ): Promise<GetExerciseDto> {
        return (
            await this.exercisesService.update(exerciseId, updateExerciseDto)
        ).toDto();
    }

    @Delete('/:exerciseId')
    async delete(@Param('exerciseId') exerciseId: number): Promise<void> {
        await this.exercisesService.delete(exerciseId);
    }
}
