import {
    Column,
    CreateDateColumn,
    DeleteDateColumn,
    Entity,
    JoinColumn,
    ManyToOne,
    OneToMany,
    OneToOne,
    PrimaryGeneratedColumn,
    UpdateDateColumn,
} from 'typeorm';
import { Post } from '../../posts/entities/post.entity';
import { User } from '../../../users/entities/user.entity';
import { GetCommentDto } from '../dto/get-comment.dto';
import { Code } from '../../../code/entities/code.entity';
import { Vote } from '../../votes/entities/vote.entity';

@Entity({ name: 'comments' })
export class Comment {
    @PrimaryGeneratedColumn('increment') id: number;

    @Column({ name: 'content' }) content: string;

    @ManyToOne(() => Post, (post) => post.comments)
    @JoinColumn({ name: 'post_id' })
    public post: Post;

    @ManyToOne(() => User, (user) => user.comments, {
        eager: true,
        cascade: true,
    })
    @JoinColumn({ name: 'user_id' })
    public author: User;

    @OneToOne(() => Code, (code) => code.comment)
    public codesProposition: Code;

    @OneToMany(() => Comment, (comment) => comment.replyTo)
    public replies: Comment[];

    @ManyToOne(() => Comment, (comment) => comment.replies)
    public replyTo: Comment;

    @OneToMany(() => Vote, (vote: Vote) => vote.comment, {
        cascade: true,
    })
    public votes: Vote[];

    public score(): number {
        return this.votes
            ? this.votes
                  .map((vote) => vote.value)
                  .reduce(
                      (accumulation, voteValue) => accumulation + voteValue,
                      0,
                  )
            : 0;
    }

    @CreateDateColumn({
        name: 'created_at',
    })
    public createdAt: Date;
    @UpdateDateColumn({
        name: 'updated_at',
    })
    public updatedAt: Date;
    @DeleteDateColumn({
        name: 'deleted_at',
        nullable: true,
    })
    public deletedAt: Date;

    toDto(userRequestingId: number): GetCommentDto {
        return {
            id: this.id,
            content: this.content,
            post: this.post?.toDto(),
            author: this.author?.toDto(),
            score: this.score(),
            codeProposition: this.codesProposition?.toDto(),
            hasUserUpVoted: this.votes
                ? this.votes.some(
                      (vote) =>
                          vote.user?.id == userRequestingId && vote.value == 1,
                  )
                : false,
            hasUserDownVoted: this.votes
                ? this.votes.some(
                      (vote) =>
                          vote.user?.id == userRequestingId && vote.value == -1,
                  )
                : false,
            createdAt: this.createdAt,
            updatedAt: this.updatedAt,
        };
    }
}
