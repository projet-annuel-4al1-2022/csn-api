import { IsNotEmpty, ValidateNested } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { CreateCodeDto } from '../../../code/dto/create-code.dto';
import { Type } from 'class-transformer';

export class CreateCommentDto {
    @IsNotEmpty() readonly content: string;
    @IsNotEmpty() readonly authorId: number;
    @IsNotEmpty() readonly postId: number;
    @ApiProperty()
    @ValidateNested()
    @Type(() => CreateCodeDto)
    readonly code?: CreateCodeDto;
}
