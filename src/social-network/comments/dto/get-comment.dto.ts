import { GetPostDto } from '../../posts/dto/get-post.dto';
import { GetUserDto } from '../../../users/DTO/get-user.dto';
import { GetCodeDto } from '../../../code/dto/get-code.dto';

export class GetCommentDto {
    readonly id: number;
    readonly content: string;
    readonly post: GetPostDto;
    readonly author: GetUserDto;
    readonly codeProposition: GetCodeDto;
    readonly score: number;
    readonly hasUserUpVoted: boolean;
    readonly hasUserDownVoted: boolean;
    readonly createdAt: Date;
    readonly updatedAt: Date;
}
