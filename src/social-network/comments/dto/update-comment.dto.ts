export class UpdateCommentDto {
    readonly content: string;
    readonly authorId: number;
}