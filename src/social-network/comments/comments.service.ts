import { forwardRef, Inject, Injectable } from '@nestjs/common';
import { CreateCommentDto } from './dto/create-comment.dto';
import { UpdateCommentDto } from './dto/update-comment.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Comment } from './entities/comment.entity';
import { PostsService } from '../posts/posts.service';
import { UsersService } from '../../users/users.service';
import { Vote } from '../votes/entities/vote.entity';
import { VotesService } from '../votes/votes.service';
import { CodesService } from '../../code/codes.service';
import { Code } from '../../code/entities/code.entity';

@Injectable()
export class CommentsService {
    constructor(
        @InjectRepository(Comment)
        private readonly commentsRepository: Repository<Comment>,
        @Inject(forwardRef(() => PostsService))
        private readonly postsService: PostsService,
        @Inject(forwardRef(() => UsersService))
        private readonly usersService: UsersService,
        @Inject(forwardRef(() => VotesService))
        private readonly votesService: VotesService,
        @Inject(forwardRef(() => CodesService))
        private readonly codesService: CodesService,
    ) {}

    async create(createCommentDto: CreateCommentDto): Promise<Comment> {
        if (!createCommentDto.authorId) {
            throw new Error("Author's id is required to create a Comment");
        }
        if (!createCommentDto.postId) {
            throw new Error("Post's id is required to create a Comment");
        }
        const post = await this.postsService.findById(createCommentDto.postId);
        const user = await this.usersService.findById(
            createCommentDto.authorId,
        );
        let createdCode: Code;
        if (createCommentDto.code) {
            createdCode = await this.codesService.create({
                code: createCommentDto.code.code,
                language: createCommentDto.code.language,
                title: createCommentDto.code.title,
            });
        }
        const savedComment = await this.commentsRepository.save({
            content: createCommentDto.content,
            post,
            author: user,
            codesProposition: createdCode,
        });
        return this.findOne(savedComment.id);
    }

    findAll(): Promise<Comment[]> {
        return this.commentsRepository.find();
    }

    findOne(commentId: number): Promise<Comment> {
        return this.commentsRepository.findOne(
            { id: commentId },
            {
                relations: ['post', 'author', 'votes'],
            },
        );
    }

    async update(commentId: number, updateCommentDto: UpdateCommentDto) {
        const toUpdate = await this.commentsRepository.findOneOrFail(commentId);
        return await this.commentsRepository.save({
            ...toUpdate,
            ...updateCommentDto,
        });
    }

    remove(commentId: number): Promise<any> {
        return this.commentsRepository.delete(commentId);
    }

    async upVote(commentId: number, userId: number): Promise<Vote> {
        return this.votesService.createOrUpdateCommentVote({
            commentId,
            userId,
            isUpvote: true,
        });
    }

    async downVote(commentId: number, userId: number): Promise<Vote> {
        return this.votesService.createOrUpdateCommentVote({
            commentId,
            userId,
            isUpvote: false,
        });
    }

    async getCommentsByPostId(postId: number): Promise<Comment[]> {
        // Get comments by post id with comment author
        return this.commentsRepository
            .createQueryBuilder('comments')
            .where('comments.post.id = :postId', { postId })
            .leftJoinAndSelect('comments.post', 'post')
            .leftJoinAndSelect('comments.author', 'author')
            .leftJoinAndSelect('comments.votes', 'votes')
            .leftJoinAndSelect('comments.codesProposition', 'codesProposition')
            .getMany();
        // return this.commentsRepository.find({
        //     where: { post: { id: postId } },
        //     relations: ['author'],
        // });
    }

    async getPostComments(postId: number) {
        return (await this.getCommentsByPostId(postId)).sort((a, b) => {
            if (a.createdAt > b.createdAt) {
                return -1;
            }
            if (a.createdAt < b.createdAt) {
                return 1;
            }
            return 0;
        });
    }
}
