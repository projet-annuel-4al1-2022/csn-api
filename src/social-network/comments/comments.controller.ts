import {
    Body,
    Controller,
    Delete,
    Get,
    Param,
    Patch,
    Post,
} from '@nestjs/common';
import { CommentsService } from './comments.service';
import { CreateCommentDto } from './dto/create-comment.dto';
import { UpdateCommentDto } from './dto/update-comment.dto';
import { GetVoteDto } from '../votes/dto/get-vote.dto';

@Controller('comments')
export class CommentsController {
    constructor(private readonly commentsService: CommentsService) {}

    @Post()
    create(@Body() createCommentDto: CreateCommentDto) {
        return this.commentsService.create(createCommentDto);
    }

    @Get()
    findAll() {
        return this.commentsService.findAll();
    }

    @Get(':commentId')
    findOne(@Param('commentId') commentId: string) {
        return this.commentsService.findOne(+commentId);
    }

    @Patch(':commentId')
    update(
        @Param('commentId') commentId: string,
        @Body() updateCommentDto: UpdateCommentDto,
    ) {
        return this.commentsService.update(+commentId, updateCommentDto);
    }

    @Delete(':commentId')
    remove(@Param('commentId') commentId: string) {
        return this.commentsService.remove(+commentId);
    }

    @Post('/:commentId/upVote/:userId')
    async upVote(
        @Param('commentId') commentId: number,
        @Param('userId') userId: number,
    ): Promise<GetVoteDto> {
        return (await this.commentsService.upVote(commentId, userId)).toDto();
    }

    @Post('/:commentId/downVote/:userId')
    async downVote(
        @Param('commentId') commentId: number,
        @Param('userId') userId: number,
    ): Promise<GetVoteDto> {
        return (await this.commentsService.downVote(commentId, userId)).toDto();
    }
}
