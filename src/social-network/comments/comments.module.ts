import { forwardRef, Module } from '@nestjs/common';
import { CommentsService } from './comments.service';
import { CommentsController } from './comments.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Comment } from './entities/comment.entity';
import { UsersModule } from '../../users/users.module';
import { PostsModule } from '../posts/posts.module';
import { VotesModule } from '../votes/votes.module';
import { CodesModule } from '../../code/codes.module';

@Module({
    imports: [
        TypeOrmModule.forFeature([Comment]),
        forwardRef(() => UsersModule),
        forwardRef(() => PostsModule),
        forwardRef(() => VotesModule),
        forwardRef(() => CodesModule),
    ],
    controllers: [CommentsController],
    providers: [CommentsService],
    exports: [CommentsService],
})
export class CommentsModule {}
