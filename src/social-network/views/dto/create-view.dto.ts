import { IsNotEmpty } from 'class-validator';

export class CreateViewDto {
    @IsNotEmpty() readonly viewerId: number;
    @IsNotEmpty() readonly postId: number;
}
