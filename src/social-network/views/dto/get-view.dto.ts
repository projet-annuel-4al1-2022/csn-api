import { UserIdDto } from '../../../users/DTO/user-id.dto';
import { PostIdDto } from '../../posts/dto/post-id.dto';

export class GetViewDto {
    public viewer: UserIdDto;
    public post: PostIdDto;
    public viewedTimes: number;
    public viewedAt: Date;
    public updatedAt: Date;
}
