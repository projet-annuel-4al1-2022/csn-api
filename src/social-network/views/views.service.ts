import { forwardRef, Inject, Injectable } from '@nestjs/common';
import { CreateViewDto } from './dto/create-view.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { View } from './entities/view.entity';
import { UsersService } from '../../users/users.service';
import { PostsService } from '../posts/posts.service';

@Injectable()
export class ViewsService {
    constructor(
        @InjectRepository(View)
        private readonly viewsRepository: Repository<View>,
        @Inject(forwardRef(() => UsersService))
        private readonly usersService: UsersService,
        @Inject(forwardRef(() => PostsService))
        private readonly postsService: PostsService,
    ) {}

    async create(createViewDto: CreateViewDto): Promise<View> {
        if (!createViewDto.viewerId) {
            throw new Error(
                "Viewer's id is required to create a View on a Post",
            );
        }
        const viewer = await this.usersService.findById(createViewDto.viewerId);
        if (!viewer) {
            throw new Error(
                'Viewer not found with id: ' + createViewDto.viewerId,
            );
        }
        if (!createViewDto.postId) {
            throw new Error("Post's id is required to create a View on a Post");
        }
        const post = await this.postsService.findById(createViewDto.postId);
        if (!post) {
            throw new Error('Post not found with id: ' + createViewDto.postId);
        }
        const view = this.viewsRepository.create({
            viewer,
            post,
        });
        await this.viewsRepository.save(view);
        return view;
    }

    findAll() {
        return this.viewsRepository.find();
    }

    async findViewByPostAndViewer(
        postId: number,
        viewerId: number,
    ): Promise<View> {
        if (!(await this.postsService.findById(postId))) {
            throw new Error('Post not found with id: ' + postId);
        }
        if (!(await this.usersService.findById(viewerId))) {
            throw new Error('Viewer not found with id: ' + viewerId);
        }
        return this.viewsRepository
            .createQueryBuilder('view')
            .innerJoinAndSelect('view.viewer', 'viewer')
            .innerJoinAndSelect('view.post', 'post')
            .where('viewer.id = :viewerId', { viewerId })
            .andWhere('post.id = :postId', { postId })
            .getOne();
    }

    updateViewedTimes(view: View) {
        return this.viewsRepository.save({
            ...view,
            viewedTimes: view.viewedTimes + 1,
        });
    }

    async createOrUpdate(createViewDto: CreateViewDto): Promise<View> {
        const view = await this.findViewByPostAndViewer(
            createViewDto.postId,
            createViewDto.viewerId,
        );
        if (view) {
            return this.updateViewedTimes(view);
        } else {
            return this.create({
                postId: createViewDto.postId,
                viewerId: createViewDto.viewerId,
            });
        }
    }

    async findByPostId(postId: number): Promise<View[]> {
        return this.viewsRepository
            .createQueryBuilder('view')
            .innerJoinAndSelect('view.viewer', 'viewer')
            .innerJoinAndSelect('view.post', 'post')
            .where('post.id = :postId', { postId })
            .getMany();
    }
}
