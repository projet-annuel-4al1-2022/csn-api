import { Column, Entity, Index, JoinColumn, ManyToOne } from 'typeorm';
import { User } from '../../../users/entities/user.entity';
import { Post } from '../../posts/entities/post.entity';
import { GetViewDto } from '../dto/get-view.dto';
import { UserIdDto } from '../../../users/DTO/user-id.dto';
import { PostIdDto } from '../../posts/dto/post-id.dto';
import * as moment from 'moment';

@Entity({ name: 'views' })
@Index(['viewer', 'post'], { unique: true })
export class View {
    @ManyToOne(() => User, (user) => user.views, {
        primary: true,
        eager: true,
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
    })
    @JoinColumn({ name: 'user_id' })
    public viewer: User;

    @ManyToOne(() => Post, (post) => post.views, {
        primary: true,
        eager: true,
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
    })
    @JoinColumn({ name: 'post_id' })
    public post: Post;

    @Column({ name: 'viewed_times', default: 1 })
    public viewedTimes: number;

    @Column({
        name: 'viewed_at',
        default: moment.utc().toISOString(),
    })
    public viewedAt: string;

    @Column({
        name: 'updated_at',
        default: moment.utc().toISOString(),
        onUpdate: moment.utc().toISOString(),
    })
    public updatedAt: string;

    toDto = (): GetViewDto => {
        return {
            viewedTimes: this.viewedTimes,
            post: this.post ? new PostIdDto(this.post.id) : undefined,
            viewer: this.viewer ? new UserIdDto(this.viewer.id) : undefined,
            viewedAt: new Date(this.viewedAt),
            updatedAt: new Date(this.updatedAt),
        };
    };
}
