import { forwardRef, Module } from '@nestjs/common';
import { ViewsService } from './views.service';
import { ViewsController } from './views.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { View } from './entities/view.entity';
import { PostsModule } from '../posts/posts.module';
import { UsersModule } from '../../users/users.module';

@Module({
    controllers: [ViewsController],
    providers: [ViewsService],
    exports: [ViewsService],
    imports: [
        TypeOrmModule.forFeature([View]),
        forwardRef(() => PostsModule),
        forwardRef(() => UsersModule),
    ],
})
export class ViewsModule {}
