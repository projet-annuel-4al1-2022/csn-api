import {
    Column,
    Entity,
    Index,
    JoinColumn,
    ManyToOne,
    PrimaryGeneratedColumn,
} from 'typeorm';
import { User } from '../../../users/entities/user.entity';
import { Post } from '../../posts/entities/post.entity';
import * as moment from 'moment';
import { GetVoteDto } from '../dto/get-vote.dto';
import { Comment } from '../../comments/entities/comment.entity';

@Entity({ name: 'votes' })
@Index(['user', 'post'], { unique: true })
@Index(['user', 'comment'], { unique: true })
export class Vote {
    @PrimaryGeneratedColumn('increment') id: number;

    @Column({ type: 'int', name: 'value', nullable: false })
    public value: number;

    @ManyToOne(() => User, (user) => user.votes, {
        eager: true,
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
    })
    @JoinColumn({ name: 'user_id' })
    public user: User;

    @ManyToOne(() => Post, (post) => post.votes, {
        eager: true,
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
        nullable: true,
    })
    @JoinColumn({ name: 'post_id' })
    public post?: Post;

    @ManyToOne(() => Comment, (comment) => comment.votes, {
        primary: false,
        eager: true,
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
        nullable: true,
    })
    @JoinColumn({ name: 'comment_id' })
    public comment?: Comment;

    @Column({
        name: 'voted_at',
        default: moment.utc().toISOString(),
    })
    public votedAt: string;

    @Column({
        name: 'updated_at',
        default: moment.utc().toISOString(),
        onUpdate: moment.utc().toISOString(),
    })
    public updatedAt: string;

    toDto = (): GetVoteDto => {
        return {
            post: this.post?.toDto(),
            user: this.user?.toDto(),
            value: this.value,
            voted_at: new Date(this.votedAt),
            updatedAt: new Date(this.updatedAt),
        };
    };
}
