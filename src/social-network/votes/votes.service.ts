import { forwardRef, Inject, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Vote } from './entities/vote.entity';
import { UsersService } from '../../users/users.service';
import { PostsService } from '../posts/posts.service';
import { CreatePostVoteDto } from './dto/create-post-vote.dto';
import * as moment from 'moment';
import { CreateCommentVoteDto } from './dto/create-comment-vote.dto';
import { CommentsService } from '../comments/comments.service';

@Injectable()
export class VotesService {
    constructor(
        @InjectRepository(Vote)
        private readonly votesRepository: Repository<Vote>,
        @Inject(forwardRef(() => UsersService))
        private readonly usersService: UsersService,
        @Inject(forwardRef(() => PostsService))
        private readonly postsService: PostsService,
        @Inject(forwardRef(() => CommentsService))
        private readonly commentsService: CommentsService,
    ) {}

    async create(
        createVoteDto: CreatePostVoteDto | CreateCommentVoteDto,
    ): Promise<Vote> {
        if (!createVoteDto.userId) {
            throw new Error("User's id is required to create a Vote on a Post");
        }
        const user = await this.usersService.findById(createVoteDto.userId);
        if (!user) {
            throw new Error('User not found with id: ' + createVoteDto.userId);
        }
        if ('postId' in createVoteDto) {
            if (!createVoteDto.postId) {
                throw new Error(
                    "Post's id is required to create a Vote on a Post",
                );
            }
            const post = await this.postsService.findById(createVoteDto.postId);
            if (!post) {
                throw new Error(
                    'Post not found with id: ' + createVoteDto.postId,
                );
            }

            const vote = this.votesRepository.create({
                user,
                post,
                value: createVoteDto.isUpvote ? 1 : -1,
            });
            await this.votesRepository.save(vote);
            return vote;
        }
        if ('commentId' in createVoteDto) {
            if (!createVoteDto.commentId) {
                throw new Error(
                    "Comment's id is required to create a Vote on a Post",
                );
            }
            const comment = await this.commentsService.findOne(
                createVoteDto.commentId,
            );
            if (!comment) {
                throw new Error(
                    'Comment not found with id: ' + createVoteDto.commentId,
                );
            }

            const vote = this.votesRepository.create({
                user,
                comment,
                value: createVoteDto.isUpvote ? 1 : -1,
            });
            await this.votesRepository.save(vote);
            return vote;
        }
    }

    async findAll(): Promise<Vote[]> {
        return this.votesRepository.find();
    }

    async findByUserAndPost(userId: number, postId: number): Promise<Vote> {
        if (!(await this.postsService.findById(postId))) {
            throw new Error('Post not found with id: ' + postId);
        }
        if (!(await this.usersService.findById(userId))) {
            throw new Error('User not found with id: ' + userId);
        }
        return this.votesRepository
            .createQueryBuilder('vote')
            .where('user.id = :userId', { userId })
            .andWhere('post.id = :postId', { postId })
            .leftJoinAndSelect('vote.user', 'user')
            .leftJoinAndSelect('vote.post', 'post')
            .leftJoinAndSelect('post.votes', 'votes')
            .leftJoinAndSelect('post.views', 'views')
            .getOne();
    }

    async findByUserAndComment(
        userId: number,
        commentId: number,
    ): Promise<Vote> {
        if (!(await this.commentsService.findOne(commentId))) {
            throw new Error('Comment not found with id: ' + commentId);
        }
        if (!(await this.usersService.findById(userId))) {
            throw new Error('User not found with id: ' + userId);
        }
        return this.votesRepository
            .createQueryBuilder('vote')
            .where('user.id = :userId', { userId })
            .andWhere('comment.id = :commentId', { commentId: commentId })
            .leftJoinAndSelect('vote.user', 'user')
            .leftJoinAndSelect('vote.comment', 'comment')
            .leftJoinAndSelect('comment.votes', 'votes')
            .getOne();
    }

    async findByPost(postId: number): Promise<Vote[]> {
        if (!(await this.postsService.findById(postId))) {
            throw new Error('Post not found with id: ' + postId);
        }
        return this.votesRepository
            .createQueryBuilder('vote')
            .leftJoinAndSelect('vote.post', 'post')
            .leftJoinAndSelect('vote.user', 'user')
            .leftJoinAndSelect('post.votes', 'votes')
            .leftJoinAndSelect('post.views', 'views')
            .where('post.id = :postId', { postId })
            .getMany();
    }

    async updateVote(vote: Vote, isUpvote: boolean): Promise<Vote> {
        if (isUpvote) {
            vote.value = vote.value >= 1 ? 1 : vote.value + 1;
        } else {
            vote.value = vote.value <= -1 ? -1 : vote.value - 1;
        }
        const savedVote = await this.votesRepository.save({
            ...vote,
            updatedAt: moment().format(),
        });
        if (vote.comment) {
            return this.findByUserAndComment(
                savedVote.user.id,
                savedVote.comment.id,
            );
        }
        if (vote.post) {
            return this.findByUserAndPost(savedVote.user.id, savedVote.post.id);
        }
    }

    async createOrUpdatePostVote(
        createVoteDto: CreatePostVoteDto,
    ): Promise<Vote> {
        const vote = await this.findByUserAndPost(
            createVoteDto.userId,
            createVoteDto.postId,
        );
        if (vote) {
            return this.updateVote(vote, createVoteDto.isUpvote);
        } else {
            return this.create(createVoteDto);
        }
    }

    async createOrUpdateCommentVote(
        createVoteDto: CreateCommentVoteDto,
    ): Promise<Vote> {
        const vote = await this.findByUserAndComment(
            createVoteDto.userId,
            createVoteDto.commentId,
        );
        if (vote) {
            return this.updateVote(vote, createVoteDto.isUpvote);
        } else {
            return this.create(createVoteDto);
        }
    }
}
