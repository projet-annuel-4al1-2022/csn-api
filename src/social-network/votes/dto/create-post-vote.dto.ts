import { IsNotEmpty } from 'class-validator';

export class CreatePostVoteDto {
    @IsNotEmpty() readonly userId: number;
    @IsNotEmpty() readonly postId: number;
    @IsNotEmpty() readonly isUpvote: boolean;
}
