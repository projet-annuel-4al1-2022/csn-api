import { IsNotEmpty } from 'class-validator';

export class CreateCommentVoteDto {
    @IsNotEmpty() readonly userId: number;
    @IsNotEmpty() readonly commentId: number;
    @IsNotEmpty() readonly isUpvote: boolean;
}
