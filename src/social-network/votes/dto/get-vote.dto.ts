import { GetPostDto } from '../../posts/dto/get-post.dto';
import { GetUserDto } from '../../../users/DTO/get-user.dto';

export class GetVoteDto {
    public user: GetUserDto;
    public post: GetPostDto;
    public value: number;
    public voted_at: Date;
    public updatedAt: Date;
}
