import { forwardRef, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Vote } from './entities/vote.entity';
import { VotesService } from './votes.service';
import { PostsModule } from '../posts/posts.module';
import { UsersModule } from '../../users/users.module';
import { CommentsModule } from '../comments/comments.module';

@Module({
    providers: [VotesService],
    exports: [VotesService],
    imports: [
        TypeOrmModule.forFeature([Vote]),
        forwardRef(() => PostsModule),
        forwardRef(() => UsersModule),
        forwardRef(() => CommentsModule),
    ],
})
export class VotesModule {}
