import {
    Body,
    Controller,
    Delete,
    Get,
    Param,
    Patch,
    Post,
    Query,
} from '@nestjs/common';
import { PostsService } from './posts.service';
import { CreatePostDto } from './dto/create-post.dto';
import { UpdatePostDto } from './dto/update-post.dto';
import { GetViewDto } from '../views/dto/get-view.dto';
import { GetVoteDto } from '../votes/dto/get-vote.dto';
import { GetPostDto } from './dto/get-post.dto';
import { GetCommentDto } from '../comments/dto/get-comment.dto';
import { CommentsService } from '../comments/comments.service';

@Controller('posts')
export class PostsController {
    constructor(
        private readonly postsService: PostsService,
        private readonly commentsService: CommentsService,
    ) {}

    // Search post by name and description
    @Get('search')
    async search(@Query('query') query: string): Promise<GetPostDto[]> {
        return (await this.postsService.search(query, 10)).map((post) =>
            post.toDto(),
        );
    }

    @Post()
    async create(@Body() createPostDto: CreatePostDto): Promise<GetPostDto> {
        return (await this.postsService.create(createPostDto)).toDto();
    }

    @Get()
    async findAll() {
        return (await this.postsService.findAll()).map((post) => post.toDto());
    }

    @Get(':postId')
    async findOne(
        @Param('postId') postId: number,
        @Query('userId') userId?: number,
    ): Promise<GetPostDto> {
        return (await this.postsService.findById(postId)).toDto(userId);
    }

    @Patch(':postId')
    async update(
        @Param('postId') postId: string,
        @Body() updatePostDto: UpdatePostDto,
    ) {
        return (await this.postsService.update(+postId, updatePostDto)).toDto();
    }

    @Delete(':postId')
    async remove(@Param('postId') postId: number) {
        await this.postsService.remove(postId);
        return {
            message: `Post successfully deleted ${postId}`,
        };
    }

    @Patch(':postId/viewedBy/:userId')
    viewedBy(@Param('postId') postId: number, @Param('userId') userId: number) {
        return this.postsService.viewedBy(postId, userId);
    }

    @Get('/:postId/views')
    async views(@Param('postId') postId: number): Promise<{
        countViews: number;
        views: GetViewDto[];
    }> {
        return {
            countViews: (await this.postsService.findById(postId)).countViews(),
            views: (await this.postsService.views(postId)).map((view) =>
                view.toDto(),
            ),
        };
    }

    @Get('/:postId/votes')
    async votes(@Param('postId') postId: number): Promise<{
        score: number;
        votes: GetVoteDto[];
    }> {
        return {
            score: await this.postsService.score(postId),
            votes: (await this.postsService.votes(postId)).map((vote) =>
                vote.toDto(),
            ),
        };
    }

    @Post('/:postId/upVote/:userId')
    async upVote(
        @Param('postId') postId: number,
        @Param('userId') userId: number,
    ): Promise<GetVoteDto> {
        return (await this.postsService.upVote(postId, userId)).toDto();
    }

    @Post('/:postId/downVote/:userId')
    async downVote(
        @Param('postId') postId: number,
        @Param('userId') userId: number,
    ): Promise<GetVoteDto> {
        return (await this.postsService.downVote(postId, userId)).toDto();
    }

    @Get('/:postId/comments')
    async comments(
        @Param('postId') postId: number,
        @Query('userId') userId: number,
    ): Promise<{
        countComments: number;
        comments: GetCommentDto[];
    }> {
        const postComments = await this.commentsService.getPostComments(postId);
        return {
            countComments: (await this.postsService.findById(postId)).comments
                ?.length,
            comments: postComments.map((comment) => comment.toDto(userId)),
        };
    }
}
