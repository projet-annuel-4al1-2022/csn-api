import { Injectable } from '@nestjs/common';
import { CreatePostDto } from './dto/create-post.dto';
import { UpdatePostDto } from './dto/update-post.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, SelectQueryBuilder } from 'typeorm';
import { Post } from './entities/post.entity';
import { UsersService } from '../../users/users.service';
import { ViewsService } from '../views/views.service';
import { GetViewDto } from '../views/dto/get-view.dto';
import { View } from '../views/entities/view.entity';
import { VotesService } from '../votes/votes.service';
import { Vote } from '../votes/entities/vote.entity';
import * as moment from 'moment';
import { CodesService } from '../../code/codes.service';
import { Comment } from '../comments/entities/comment.entity';

@Injectable()
export class PostsService {
    constructor(
        @InjectRepository(Post)
        private readonly postsRepository: Repository<Post>,
        private readonly usersService: UsersService,
        private readonly viewsService: ViewsService,
        private readonly votesService: VotesService,
        private readonly codesService: CodesService,
    ) {}

    async search(search: string, limit: number): Promise<Post[]> {
        search = search.toLowerCase();
        return (
            this.postsRepository
                .createQueryBuilder('post')
                // field lower case
                .where('LOWER(post.title) LIKE :search', {
                    search: `%${search}%`,
                })
                // field lower case
                .orWhere('LOWER(post.content) LIKE :search', {
                    search: `%${search}%`,
                })
                .orderBy('post.createdAt', 'DESC')
                .take(limit)
                .getMany()
        );
    }

    async create(createPostDto: CreatePostDto): Promise<Post> {
        if (!createPostDto.authorId) {
            throw new Error("Author's id is required to create a Post");
        }
        const author = await this.usersService.findById(createPostDto.authorId);
        if (!author) {
            throw new Error(
                'Author not found with id: ' + createPostDto.authorId,
            );
        }
        const { codes, ...post } = createPostDto;
        const createdPost = await this.postsRepository.create({
            ...post,
            author,
        });
        const savedPost = await this.postsRepository.save(createdPost);
        if (codes) {
            await this.codesService.createMultiple(
                codes.map((code) => ({
                    ...code,
                    post: savedPost,
                })),
            );
        }
        return this.findById(savedPost.id);
    }

    addLeftJoinAndSelectForPosts(
        queryBuilder: SelectQueryBuilder<Post>,
    ): SelectQueryBuilder<Post> {
        return queryBuilder
            .leftJoinAndSelect('post.author', 'author')
            .leftJoinAndSelect('post.codes', 'codes')
            .leftJoinAndSelect('post.votes', 'votes')
            .leftJoinAndSelect('post.comments', 'comments')
            .leftJoinAndSelect('post.views', 'views')
            .leftJoinAndSelect('votes.user', 'votes.user')
            .leftJoinAndSelect('comments.author', 'comments.author')
            .leftJoinAndSelect('views.viewer', 'views.viewer');
    }

    findAll(): Promise<Post[]> {
        return this.addLeftJoinAndSelectForPosts(
            this.postsRepository.createQueryBuilder('post'),
        ).getMany();
    }

    findById(postId: number): Promise<Post> {
        return this.addLeftJoinAndSelectForPosts(
            this.postsRepository
                .createQueryBuilder('post')
                .where('post.id = :id', { id: postId }),
        ).getOneOrFail();
    }

    async update(postId: number, updatePostDto: UpdatePostDto): Promise<Post> {
        if (
            updatePostDto.authorId &&
            !(await this.usersService.findById(updatePostDto.authorId))
        ) {
            throw new Error(
                `Author with id ${updatePostDto.authorId} does not exist`,
            );
        }
        const toUpdate = await this.postsRepository.findOneOrFail(postId);
        const saved = await this.postsRepository.save({
            ...toUpdate,
            ...updatePostDto,
            updatedAt: moment().format(),
        });
        return this.findById(saved.id);
    }

    remove(postId: number): Promise<any> {
        return this.postsRepository.delete(postId);
    }

    async viewedBy(postId: number, userId: number): Promise<GetViewDto> {
        const post = await this.findById(postId);
        const view = await this.viewsService.createOrUpdate({
            postId: postId,
            viewerId: userId,
        });
        post.views.push(view);
        await this.postsRepository.save(post);
        return (
            await this.viewsService.findViewByPostAndViewer(postId, userId)
        )?.toDto();
    }

    async views(postId: number): Promise<View[]> {
        return this.viewsService.findByPostId(postId);
    }

    async votes(postId: number) {
        return this.votesService.findByPost(postId);
    }

    async score(postId: number): Promise<number> {
        return (await this.findById(postId)).score();
    }

    async upVote(postId: number, userId: number): Promise<Vote> {
        return this.votesService.createOrUpdatePostVote({
            postId,
            userId,
            isUpvote: true,
        });
    }

    async downVote(postId: number, userId: number): Promise<Vote> {
        return this.votesService.createOrUpdatePostVote({
            postId,
            userId,
            isUpvote: false,
        });
    }

    async getUserFeed(
        userIds: number[],
        nowDate: Date,
        limit: number,
    ): Promise<Post[]> {
        return this.addLeftJoinAndSelectForPosts(
            this.postsRepository
                .createQueryBuilder('post')
                .where('post.createdAt <= :nowDate', { nowDate })
                .andWhere('post.user_id IN (:...userIds)', { userIds })
                .orderBy('post.createdAt', 'DESC'),
        )
            .take(limit)
            .getMany();
    }

    sortUsersFeed(posts: Post[]): Post[] {
        return posts.sort((a, b) => {
            const aMoment = moment.utc(a.createdAt);
            const bMoment = moment.utc(b.createdAt);
            if (aMoment.isSame(bMoment, 'minute')) {
                return b.score() - a.score();
            }
            if (a.createdAt > b.createdAt) {
                return -1;
            }
            if (a.createdAt < b.createdAt) {
                return 1;
            }
            return 0;
        });
    }

    async trends(userId: number): Promise<Post[]> {
        return this.addLeftJoinAndSelectForPosts(
            this.postsRepository
                .createQueryBuilder('post')
                .where('post.user_id != :userId', { userId })
                .addSelect((subQuery) => {
                    return subQuery
                        .select(
                            'COALESCE(SUM(vote.value), 1) * COALESCE(SUM(view.viewed_times), 1)',
                            'score',
                        )
                        .from(Vote, 'vote')
                        .from(View, 'view');
                }, 'score')
                .andWhere('post.created_at >= :lastSevenDaysDate', {
                    lastSevenDaysDate: moment().subtract(4, 'week').format(),
                })
                .orderBy('score', 'DESC')
                .limit(15),
        ).getMany();
    }

    async getPostComments(postId: number): Promise<Comment[]> {
        // Sort comment by createdAt DESC
        return (await this.findById(postId)).comments.sort((a, b) => {
            if (a.createdAt > b.createdAt) {
                return -1;
            }
            if (a.createdAt < b.createdAt) {
                return 1;
            }
            return 0;
        });
    }

    async getUserPosts(userId: number) {
        return this.addLeftJoinAndSelectForPosts(
            this.postsRepository
                .createQueryBuilder('post')
                .where('post.user_id = :userId', { userId })
                .orderBy('post.createdAt', 'DESC'),
        ).getMany();
    }
}
