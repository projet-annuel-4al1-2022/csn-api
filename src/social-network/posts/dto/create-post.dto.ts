import { IsNotEmpty, ValidateNested } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { CreateCodeDto } from '../../../code/dto/create-code.dto';
import { Type } from 'class-transformer';

export class CreatePostDto {
    @ApiProperty()
    @IsNotEmpty()
    readonly title: string;
    @ApiProperty()
    @IsNotEmpty()
    readonly content: string;
    @ApiProperty()
    @ValidateNested({ each: true })
    @Type(() => CreateCodeDto)
    readonly codes: CreateCodeDto[];
    @ApiProperty()
    @IsNotEmpty()
    readonly authorId: number;
}
