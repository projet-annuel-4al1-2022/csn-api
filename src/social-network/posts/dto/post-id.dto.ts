import { ApiProperty } from '@nestjs/swagger';

export class PostIdDto {
    @ApiProperty()
    public postId: number;

    constructor(postId: number) {
        this.postId = postId;
    }
}
