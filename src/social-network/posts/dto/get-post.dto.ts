import { GetViewDto } from '../../views/dto/get-view.dto';
import { GetVoteDto } from '../../votes/dto/get-vote.dto';
import { GetUserDto } from '../../../users/DTO/get-user.dto';
import { GetCodeDto } from '../../../code/dto/get-code.dto';
import { GetCommentDto } from '../../comments/dto/get-comment.dto';

export class GetPostDto {
    readonly id: number;
    readonly title: string;
    readonly content: string;
    readonly codes: GetCodeDto[];
    readonly author: GetUserDto;
    readonly votes: GetVoteDto[];
    readonly score: number;
    readonly comments: GetCommentDto[];
    readonly countComments: number;
    readonly views: GetViewDto[];
    readonly countViews: number;
    readonly createdAt: Date;
    readonly updatedAt: Date;
    readonly hasUserUpVoted: boolean;
    readonly hasUserDownVoted: boolean;
}
