import { forwardRef, Module } from '@nestjs/common';
import { PostsService } from './posts.service';
import { PostsController } from './posts.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Post } from './entities/post.entity';
import { UsersModule } from '../../users/users.module';
import { ViewsModule } from '../views/views.module';
import { VotesModule } from '../votes/votes.module';
import { CodesModule } from '../../code/codes.module';
import { CommentsModule } from '../comments/comments.module';

@Module({
    controllers: [PostsController],
    providers: [PostsService],
    exports: [PostsService],
    imports: [
        TypeOrmModule.forFeature([Post]),
        forwardRef(() => ViewsModule),
        forwardRef(() => UsersModule),
        forwardRef(() => VotesModule),
        forwardRef(() => CodesModule),
        forwardRef(() => CommentsModule),
    ],
})
export class PostsModule {}
