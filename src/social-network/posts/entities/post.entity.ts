import {
    Column,
    CreateDateColumn,
    DeleteDateColumn,
    Entity,
    JoinColumn,
    ManyToOne,
    OneToMany,
    PrimaryGeneratedColumn,
    UpdateDateColumn,
} from 'typeorm';
import { User } from '../../../users/entities/user.entity';
import { View } from '../../views/entities/view.entity';
import { Vote } from '../../votes/entities/vote.entity';
import { GetPostDto } from '../dto/get-post.dto';
import { Code } from '../../../code/entities/code.entity';
import { Comment } from '../../comments/entities/comment.entity';

@Entity({ name: 'posts' })
export class Post {
    @PrimaryGeneratedColumn('increment')
    public id: number;

    @Column({ name: 'title' })
    public title: string;

    @Column({ name: 'content' })
    public content: string;

    @OneToMany(() => Code, (code: Code) => code.post, {
        eager: true,
        cascade: false,
    })
    public codes: Code[];

    @ManyToOne(() => User, (user) => user.posts, {
        eager: true,
        cascade: true,
    })
    @JoinColumn({ name: 'user_id' })
    public author: User;

    @OneToMany(() => Vote, (vote: Vote) => vote.post, {
        cascade: true,
    })
    public votes: Vote[];

    public score(): number {
        return this.votes
            ? this.votes
                  .map((vote) => vote.value)
                  .reduce(
                      (accumulation, voteValue) => accumulation + voteValue,
                      0,
                  ) //  * (this.countViews() || 1)
            : undefined;
    }

    @OneToMany(() => View, (view) => view.post, {
        cascade: true,
    })
    public views: View[];

    public countViews(): number {
        return this.views
            ? this.views
                  .map((view) => view.viewedTimes)
                  .reduce(
                      (accumulator, viewedTimes) => viewedTimes + accumulator,
                      0,
                  )
            : 0;
    }

    @OneToMany(() => Comment, (comment) => comment.post, {
        cascade: true,
    })
    public comments: Comment[];

    @CreateDateColumn({
        type: 'timestamptz',
        name: 'created_at',
    })
    public createdAt: Date;

    @UpdateDateColumn({
        type: 'timestamptz',
        name: 'updated_at',
    })
    public updatedAt: Date;

    @DeleteDateColumn({
        type: 'timestamptz',
        name: 'deleted_at',
        nullable: true,
    })
    public deletedAt: Date;

    public toDto = (userRequestingId?: number): GetPostDto => {
        return {
            id: this.id,
            title: this.title,
            content: this.content,
            codes: this.codes
                ? this.codes.map((code) => code.toDto())
                : undefined,
            author: this.author?.toDto(),
            votes: this.votes
                ? this.votes
                      .filter((vote) => vote.votedAt)
                      .map((vote) => vote.toDto())
                : undefined,
            score: this.score(),
            views: this.views
                ? this.views
                      .filter((view) => view.viewedAt)
                      .map((view) => view.toDto())
                : undefined,
            countViews: this.countViews(),
            comments: this.comments
                ? this.comments.map((comment) =>
                      comment.toDto(userRequestingId),
                  )
                : undefined,
            countComments: this.comments?.length > 0 ? this.comments.length : 0,
            createdAt: this.createdAt,
            updatedAt: this.updatedAt,
            hasUserUpVoted: this.votes
                ? this.votes.some(
                      (vote) =>
                          vote.user?.id == userRequestingId && vote.value > 0,
                  )
                : false,
            hasUserDownVoted: this.votes
                ? this.votes.some(
                      (vote) =>
                          vote.user?.id == userRequestingId && vote.value < 0,
                  )
                : false,
        };
    };
}
