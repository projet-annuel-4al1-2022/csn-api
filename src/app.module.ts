import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { config } from 'dotenv';
import { configService } from './config/config.service';
import { UsersModule } from './users/users.module';
import { AuthModule } from './auth/auth.module';
import { MailModule } from './mail/mail.module';
import { ConfigModule } from '@nestjs/config';
import { PostsModule } from './social-network/posts/posts.module';
import { ViewsModule } from './social-network/views/views.module';
import { VotesModule } from './social-network/votes/votes.module';
import { SnippetsModule } from './code/snippets/snippets.module';
import { CodesModule } from './code/codes.module';
import { CommentsModule } from './social-network/comments/comments.module';
import { SeedersModule } from './seeders/seeders.module';
import { ExercisesModule } from './collaborative/exercises/exercises.module';
import { ValidatingTestModule } from './collaborative/validating-test/validating-test.module';
import { ChallengesModule } from './collaborative/challenges/challenges.module';
import { ChallengeGateway } from './collaborative/challenges/gateways/challenge-notification.gateway';

config();

@Module({
    imports: [
        ConfigModule.forRoot({
            isGlobal: true, // no need to import into other modules
        }),
        TypeOrmModule.forRoot(configService.getTypeOrmConfig()),
        UsersModule,
        AuthModule,
        MailModule,
        PostsModule,
        ViewsModule,
        VotesModule,
        SnippetsModule,
        CodesModule,
        CommentsModule,
        ExercisesModule,
        SeedersModule,
        ValidatingTestModule,
        ChallengesModule,
    ],
    controllers: [AppController],
    providers: [AppService, ChallengeGateway],
})
export class AppModule {}
