import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import * as moment from 'moment';
import { Code } from './entities/code.entity';
import { CreateCodeDto } from './dto/create-code.dto';
import { UpdateCodeDto } from './dto/update-code.dto';

@Injectable()
export class CodesService {
    constructor(
        @InjectRepository(Code)
        private readonly codesRepository: Repository<Code>,
    ) {}

    async create(createCodeDto: CreateCodeDto): Promise<Code> {
        const code = this.codesRepository.create({
            ...createCodeDto,
        });
        const saved = await this.codesRepository.save(code);
        return this.findById(saved.id);
    }

    async createMultiple(codes: CreateCodeDto[]): Promise<Code[]> {
        const codesToCreate = codes.map((code) => {
            return this.codesRepository.create({
                ...code,
            });
        });
        return this.codesRepository.save(codesToCreate);
    }

    findAll(): Promise<Code[]> {
        return this.codesRepository
            .createQueryBuilder('code')
            .leftJoinAndSelect('code.post', 'post')
            .leftJoinAndSelect('code.snippet', 'snippet')
            .getMany();
    }

    findById(codeId: number): Promise<Code> {
        return this.codesRepository
            .createQueryBuilder('code')
            .where('code.id = :id', { id: codeId })
            .leftJoinAndSelect('code.post', 'post')
            .leftJoinAndSelect('code.snippet', 'snippet')
            .getOneOrFail();
    }

    async update(codeId: number, updateCodeDto: UpdateCodeDto): Promise<Code> {
        const toUpdate = await this.codesRepository.findOneOrFail(codeId);
        const saved = await this.codesRepository.save({
            ...toUpdate,
            ...updateCodeDto,
            updatedAt: moment().format(),
        });
        return this.findById(saved.id);
    }

    remove(postId: number): Promise<any> {
        return this.codesRepository.delete(postId);
    }

    async save(createdCode: Code) {
        return this.codesRepository.save(createdCode);
    }
}
