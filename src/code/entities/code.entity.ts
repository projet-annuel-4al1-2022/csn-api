import {
    Column,
    CreateDateColumn,
    DeleteDateColumn,
    Entity,
    JoinColumn,
    ManyToOne,
    OneToOne,
    PrimaryGeneratedColumn,
    UpdateDateColumn,
} from 'typeorm';
import { Post } from '../../social-network/posts/entities/post.entity';
import { Snippet } from '../snippets/entities/snippet.entity';
import { GetCodeDto } from '../dto/get-code.dto';
import { Comment } from '../../social-network/comments/entities/comment.entity';
import { ChallengeInfo } from '../../collaborative/challenges/entities/challenger-info.entity';

// {
//     label: '',
//     value: '',
//     id: -1,
// },
// {
//     label: 'C (GCC 9.2.0)',
//     value: 'c_cpp',
//     id: 50,
// },
// {
//     label: 'C++ (GCC 9.2.0)',
//     value: 'c_cpp',
//     id: 54,
// },
// {
//     label: 'C#',
//     value: 'csharp',
//     id: 51,
// },
// {
//     label: 'Java (JDK 13)',
//     value: 'java',
//     id: 62,
// },
// {
//     label: 'Python (3.8.1)',
//     value: 'python',
//     id: 71,
// },

export enum Language {
    C = 'c',
    CPLUSPLUS = 'c_cpp',
    JAVA = 'java',
    CSHARP = 'csharp',
    PYTHON = 'python',
}

@Entity({ name: 'codes' })
export class Code {
    @PrimaryGeneratedColumn('increment')
    id: number;

    @Column({ type: 'varchar', length: 255, nullable: false })
    title: string;

    @Column({ type: 'varchar', nullable: false })
    code: string;

    @Column({ name: 'language', type: 'enum', enum: Language, nullable: true })
    language: Language;

    @ManyToOne(() => Post, (post) => post.codes, {
        cascade: false,
        nullable: true,
    })
    post?: Post;

    @OneToOne(() => Comment, (comment) => comment.codesProposition, {
        cascade: false,
        nullable: true,
    })
    @JoinColumn({ name: 'comment_id' })
    comment?: Comment;

    @OneToOne(() => Snippet, (snippet) => snippet.code, {
        onDelete: 'SET NULL',
        nullable: true,
    })
    snippet?: Snippet;

    @OneToOne(() => ChallengeInfo, (challengeInfo) => challengeInfo.userCode, {
        nullable: true,
    })
    challengeInfo?: ChallengeInfo;

    @CreateDateColumn({
        type: 'timestamptz',
        name: 'created_at',
    })
    public createdAt: Date;

    @UpdateDateColumn({
        type: 'timestamptz',
        name: 'updated_at',
    })
    public updatedAt: Date;

    @DeleteDateColumn({
        type: 'timestamptz',
        name: 'deleted_at',
        nullable: true,
    })
    public deletedAt: Date;

    toDto(): GetCodeDto {
        return {
            id: this.id,
            title: this.title,
            code: this.code,
            language: this.language.toString(),
            post: this.post?.toDto(),
            snippet: this.snippet?.toDto(),
            createdAt: this.createdAt,
            updatedAt: this.updatedAt,
        };
    }
}
