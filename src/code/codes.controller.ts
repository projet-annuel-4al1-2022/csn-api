import {
    Body,
    Controller,
    Delete,
    Get,
    Param,
    Patch,
    Post,
} from '@nestjs/common';
import { CodesService } from './codes.service';
import { CreateCodeDto } from './dto/create-code.dto';
import { GetCodeDto } from './dto/get-code.dto';
import { UpdateCodeDto } from './dto/update-code.dto';

@Controller('codes')
export class CodesController {
    constructor(private readonly codesService: CodesService) {}

    @Post()
    async create(@Body() createCodeDto: CreateCodeDto): Promise<GetCodeDto> {
        return (await this.codesService.create(createCodeDto)).toDto();
    }

    @Get()
    async findAll() {
        return (await this.codesService.findAll()).map((post) => post.toDto());
    }

    @Get(':codeId')
    async findOne(@Param('codeId') codeId: number) {
        return (await this.codesService.findById(codeId)).toDto();
    }

    @Patch(':codeId')
    async update(
        @Param('codeId') codeId: number,
        @Body() updateCodeDto: UpdateCodeDto,
    ) {
        return (await this.codesService.update(codeId, updateCodeDto)).toDto();
    }

    @Delete(':codeId')
    async remove(@Param('codeId') codeId: number) {
        await this.codesService.remove(codeId);
        return {
            message: `Post successfully deleted ${codeId}`,
        };
    }
}
