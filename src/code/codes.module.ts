import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Code } from './entities/code.entity';
import { CodesService } from './codes.service';
import { CodesController } from './codes.controller';

@Module({
    controllers: [CodesController],
    providers: [CodesService],
    exports: [CodesService],
    imports: [TypeOrmModule.forFeature([Code])],
})
export class CodesModule {}
