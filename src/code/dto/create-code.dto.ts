import { ApiProperty } from '@nestjs/swagger';
import { Language } from '../entities/code.entity';
import { Post } from '../../social-network/posts/entities/post.entity';
import { Snippet } from '../snippets/entities/snippet.entity';
import { IsNotEmpty } from 'class-validator';

export class CreateCodeDto {
    @ApiProperty()
    @IsNotEmpty()
    readonly title: string;
    @ApiProperty()
    @IsNotEmpty()
    readonly code: string;
    @ApiProperty()
    @IsNotEmpty()
    readonly language: Language;
    @ApiProperty()
    readonly post?: Post;
    @ApiProperty()
    readonly snippet?: Snippet;
}
