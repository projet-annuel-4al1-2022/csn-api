import { ApiProperty } from '@nestjs/swagger';
import { Language } from '../entities/code.entity';

export class UpdateCodeDto {
    @ApiProperty()
    readonly title?: string;
    @ApiProperty()
    readonly code?: string;
    @ApiProperty()
    readonly languages?: Language;
}
