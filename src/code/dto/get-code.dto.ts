import { GetPostDto } from '../../social-network/posts/dto/get-post.dto';
import { GetSnippetDto } from '../snippets/dto/get-snippet.dto';

export class GetCodeDto {
    readonly id: number;
    readonly title: string;
    readonly code: string;
    readonly language: string;
    readonly post?: GetPostDto;
    readonly snippet?: GetSnippetDto;
    readonly createdAt: Date;
    readonly updatedAt: Date;
}
