import {
    Column,
    CreateDateColumn,
    DeleteDateColumn,
    Entity,
    JoinColumn,
    ManyToOne,
    OneToOne,
    PrimaryGeneratedColumn,
    UpdateDateColumn,
} from 'typeorm';
import { Code } from '../../entities/code.entity';
import { GetSnippetDto } from '../dto/get-snippet.dto';
import { User } from '../../../users/entities/user.entity';
import { UserIdDto } from '../../../users/DTO/user-id.dto';

@Entity({ name: 'snippets' })
export class Snippet {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    title: string;

    @Column()
    description: string;

    @OneToOne(() => Code, (code: Code) => code.snippet, {
        onDelete: 'SET NULL',
        eager: true,
        cascade: false,
    })
    @JoinColumn()
    public code: Code;

    @ManyToOne(() => User, (user: User) => user.snippets, {
        cascade: false,
    })
    public user: User;

    @CreateDateColumn({
        type: 'timestamptz',
        name: 'created_at',
    })
    public createdAt: Date;

    @UpdateDateColumn({
        type: 'timestamptz',
        name: 'updated_at',
    })
    public updatedAt: Date;

    @DeleteDateColumn({
        type: 'timestamptz',
        name: 'deleted_at',
        nullable: true,
    })
    public deletedAt: Date;

    toDto(): GetSnippetDto {
        return {
            id: this.id,
            title: this.title,
            description: this.description,
            code: this.code?.toDto(),
            user: new UserIdDto(this.user.id),
            createdAt: this.createdAt,
            updatedAt: this.updatedAt,
        };
    }
}
