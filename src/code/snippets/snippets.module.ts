import { forwardRef, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Snippet } from './entities/snippet.entity';
import { SnippetsController } from './snippets.controller';
import { SnippetsService } from './snippets.service';
import { CodesModule } from '../codes.module';
import { UsersModule } from '../../users/users.module';

@Module({
    controllers: [SnippetsController],
    providers: [SnippetsService],
    exports: [SnippetsService],
    imports: [
        TypeOrmModule.forFeature([Snippet]),
        forwardRef(() => CodesModule),
        forwardRef(() => UsersModule),
    ],
})
export class SnippetsModule {}
