import {
    Body,
    Controller,
    Delete,
    Get,
    Param,
    Patch,
    Post,
} from '@nestjs/common';
import { SnippetsService } from './snippets.service';
import { CreateSnippetDto } from './dto/create-snippet.dto';
import { GetSnippetDto } from './dto/get-snippet.dto';
import { UpdateSnippetDto } from './dto/update-snippet.dto';

@Controller('snippets')
export class SnippetsController {
    constructor(private readonly snippetsService: SnippetsService) {}

    @Post()
    async create(
        @Body() createSnippetDto: CreateSnippetDto,
    ): Promise<GetSnippetDto> {
        return (await this.snippetsService.create(createSnippetDto)).toDto();
    }

    @Get()
    async findAll() {
        return (await this.snippetsService.findAll()).map((snippet) =>
            snippet.toDto(),
        );
    }

    @Get(':snippetId')
    async findOne(@Param('snippetId') snippetId: number) {
        return (await this.snippetsService.findById(snippetId)).toDto();
    }

    @Patch(':snippetId')
    async update(
        @Param('snippetId') snippetId: number,
        @Body() updateSnippetDto: UpdateSnippetDto,
    ) {
        return (
            await this.snippetsService.update(snippetId, updateSnippetDto)
        ).toDto();
    }

    @Delete(':snippetId')
    async remove(@Param('snippetId') snippetId: number) {
        await this.snippetsService.remove(snippetId);
        return {
            message: `Post successfully deleted ${snippetId}`,
        };
    }
}
