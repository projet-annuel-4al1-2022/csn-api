import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import * as moment from 'moment';
import { Snippet } from './entities/snippet.entity';
import { CreateSnippetDto } from './dto/create-snippet.dto';
import { UpdateSnippetDto } from './dto/update-snippet.dto';
import { CodesService } from '../codes.service';
import { UsersService } from '../../users/users.service';

@Injectable()
export class SnippetsService {
    constructor(
        @InjectRepository(Snippet)
        private readonly snippetsRepository: Repository<Snippet>,
        private readonly codesService: CodesService,
        private readonly usersService: UsersService,
    ) {}

    async create(createSnippetDto: CreateSnippetDto): Promise<Snippet> {
        const user = await this.usersService.findById(createSnippetDto.userId);
        if (!user) {
            throw new Error(
                `User with id ${createSnippetDto.userId} not found when creating a snippet`,
            );
        }
        const createdCode = await this.codesService.create(
            createSnippetDto.code,
        );
        const snippet = this.snippetsRepository.create({
            ...createSnippetDto,
            code: createdCode,
            user,
        });
        return this.snippetsRepository.save(snippet);
    }

    findAll(): Promise<Snippet[]> {
        return this.snippetsRepository
            .createQueryBuilder('snippet')
            .leftJoinAndSelect('snippet.user', 'user')
            .leftJoinAndSelect('snippet.code', 'code')
            .getMany();
    }

    findById(snippetId: number): Promise<Snippet> {
        return this.snippetsRepository
            .createQueryBuilder('snippet')
            .where('snippet.id = :id', { id: snippetId })
            .leftJoinAndSelect('snippet.user', 'user')
            .leftJoinAndSelect('snippet.code', 'code')
            .getOneOrFail();
    }

    async update(
        snippetId: number,
        updateSnippetDto: UpdateSnippetDto,
    ): Promise<Snippet> {
        const toUpdate = await this.snippetsRepository.findOneOrFail(snippetId);
        const codeToUpdate = await this.codesService.update(toUpdate.code.id, {
            code: updateSnippetDto.codeContent,
        });
        const saved = await this.snippetsRepository.save({
            ...toUpdate,
            ...updateSnippetDto,
            updatedAt: moment().format(),
        });
        saved.code = codeToUpdate;
        await this.codesService.save(codeToUpdate);
        await this.snippetsRepository.save(saved);
        return this.findById(saved.id);
    }

    remove(postId: number): Promise<any> {
        return this.snippetsRepository.delete(postId);
    }

    async findByUserId(userId: number): Promise<Snippet[]> {
        return this.snippetsRepository
            .createQueryBuilder('snippet')
            .where('snippet.userId = :userId', { userId })
            .leftJoinAndSelect('snippet.user', 'user')
            .leftJoinAndSelect('snippet.code', 'code')
            .getMany();
    }
}
