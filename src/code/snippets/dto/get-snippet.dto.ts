import { GetCodeDto } from '../../dto/get-code.dto';
import { UserIdDto } from '../../../users/DTO/user-id.dto';

export class GetSnippetDto {
    readonly id: number;
    readonly title: string;
    readonly description: string;
    readonly code: GetCodeDto;
    readonly user: UserIdDto;
    readonly createdAt: Date;
    readonly updatedAt: Date;
}
