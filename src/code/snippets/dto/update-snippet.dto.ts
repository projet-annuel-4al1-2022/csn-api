import { ApiProperty } from '@nestjs/swagger';

export class UpdateSnippetDto {
    @ApiProperty()
    readonly title?: string;
    @ApiProperty()
    readonly description?: string;
    @ApiProperty()
    readonly codeContent?: string;
    @ApiProperty()
    readonly codeLanguage?: string;
}
