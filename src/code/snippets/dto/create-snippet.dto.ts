import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsNotEmptyObject, ValidateNested } from 'class-validator';
import { Type } from 'class-transformer';
import { CreateCodeDto } from '../../dto/create-code.dto';

export class CreateSnippetDto {
    @ApiProperty()
    @IsNotEmpty()
    readonly title: string;

    @ApiProperty()
    readonly description: string;

    @ApiProperty()
    @IsNotEmptyObject()
    @ValidateNested({ each: true })
    @Type(() => CreateCodeDto)
    readonly code: CreateCodeDto;

    @ApiProperty()
    @IsNotEmpty()
    readonly userId: number;
}
