import {
    Body,
    Controller,
    Get,
    Post,
    Request,
    UseGuards,
} from '@nestjs/common';
import { AppService } from './app.service';
import { LocalAuthGuard } from './auth/local-auth.guard';
import { AuthService } from './auth/auth.service';
import { CreateUserDto } from './users/DTO/create-user.dto';
import { UsersService } from './users/users.service';
import { GetUserDto } from './users/DTO/get-user.dto';
import { User } from './users/entities/user.entity';

@Controller()
export class AppController {
    constructor(
        private readonly appService: AppService,
        private readonly usersService: UsersService,
        private authService: AuthService,
    ) {}

    @Get()
    getHello(): string {
        return this.appService.getHello();
    }

    @UseGuards(LocalAuthGuard)
    @Post('auth/login')
    async login(@Request() req) {
        return this.authService.login(req.user);
    }

    @Post('auth/signup')
    async register(@Body() user: CreateUserDto): Promise<GetUserDto> {
        const createdUser: User = (await this.usersService.createUser(
            user,
        )) as User;
        return createdUser.toDto();
    }
}
