import { User } from '../users/entities/user.entity';
import { Injectable } from '@nestjs/common';
import { Post } from '../social-network/posts/entities/post.entity';
import { ViewsService } from '../social-network/views/views.service';
import { View } from '../social-network/views/entities/view.entity';
import { getRandomBetween } from '../helpers/math.helper';

@Injectable()
export class ViewsSeeder {
    constructor(private readonly viewsService: ViewsService) {}

    async seed(users: User[], posts: Post[]): Promise<View[]> {
        const views = [];
        for (const post of posts) {
            // Generate a random number of view with ViewsService
            const numberOfViews = getRandomBetween(5, 9);
            for (let i = 0; i < numberOfViews; i++) {
                const view = await this.viewsService.createOrUpdate({
                    postId: post.id,
                    viewerId: users[getRandomBetween(0, users.length - 1)].id,
                });
                views.push(view);
            }
        }
        return views;
    }
}
