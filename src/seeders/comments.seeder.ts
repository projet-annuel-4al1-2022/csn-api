import { User } from '../users/entities/user.entity';
import { Injectable } from '@nestjs/common';
import { Post } from '../social-network/posts/entities/post.entity';
import { getRandomBetween } from '../helpers/math.helper';
import { CommentsService } from '../social-network/comments/comments.service';
import { Comment } from '../social-network/comments/entities/comment.entity';

@Injectable()
export class CommentsSeeder {
    constructor(private readonly commentsService: CommentsService) {}

    async seed(users: User[], posts: Post[]): Promise<Comment[]> {
        const comments = [];
        for (const post of posts) {
            // Generate a random number of comments with CommentsService
            const numberOfComments = getRandomBetween(4, 6);
            for (let i = 0; i < numberOfComments; i++) {
                const comment = await this.commentsService.create({
                    authorId: users[getRandomBetween(0, users.length - 1)].id,
                    postId: post.id,
                    content: `Comment ${i + 1}`,
                });
                comments.push(comment);
            }
        }
        return comments;
    }
}
