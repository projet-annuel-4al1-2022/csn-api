import { UsersSeeder } from './users.seeder';
import { Module } from '@nestjs/common';
import { UsersModule } from '../users/users.module';
import { AuthModule } from '../auth/auth.module';
import { MailModule } from '../mail/mail.module';
import { PostsModule } from '../social-network/posts/posts.module';
import { ViewsModule } from '../social-network/views/views.module';
import { VotesModule } from '../social-network/votes/votes.module';
import { SnippetsModule } from '../code/snippets/snippets.module';
import { CodesModule } from '../code/codes.module';
import { CommentsModule } from '../social-network/comments/comments.module';
import { GlobalSeeder } from './global.seeder';
import { PostsSeeder } from './posts.seeder';
import { ViewsSeeder } from './views.seeder';
import { VotesSeeder } from './votes.seeder';
import { CommentsSeeder } from './comments.seeder';
import { ExercisesSeeder } from './exercises.seeder';
import { ExercisesModule } from '../collaborative/exercises/exercises.module';
import { ValidatingTestModule } from '../collaborative/validating-test/validating-test.module';

@Module({
    imports: [
        UsersModule,
        AuthModule,
        MailModule,
        PostsModule,
        ViewsModule,
        VotesModule,
        SnippetsModule,
        CodesModule,
        CommentsModule,
        ExercisesModule,
        ValidatingTestModule,
        SeedersModule,
    ],
    providers: [
        GlobalSeeder,
        UsersSeeder,
        PostsSeeder,
        ViewsSeeder,
        VotesSeeder,
        CommentsSeeder,
        ExercisesSeeder,
    ],
})
export class SeedersModule {}
