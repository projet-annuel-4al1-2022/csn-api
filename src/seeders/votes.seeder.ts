import { User } from '../users/entities/user.entity';
import { Injectable } from '@nestjs/common';
import { Post } from '../social-network/posts/entities/post.entity';
import { getRandomBetween } from '../helpers/math.helper';
import { VotesService } from '../social-network/votes/votes.service';
import { Vote } from '../social-network/votes/entities/vote.entity';
import { Comment } from '../social-network/comments/entities/comment.entity';

@Injectable()
export class VotesSeeder {
    constructor(private readonly votesService: VotesService) {}

    async seedPostVotes(users: User[], posts: Post[]): Promise<Vote[]> {
        const votes = [];
        for (const user of users) {
            const willVoteOrNot = Math.random() > 0.5;
            if (willVoteOrNot) {
                const post = posts[getRandomBetween(0, posts.length - 1)];
                if (
                    !votes.find(
                        (vote) =>
                            vote.userId === user.id && vote.postId === post.id,
                    )
                ) {
                    const vote = await this.votesService.createOrUpdatePostVote(
                        {
                            postId: posts[getRandomBetween(0, posts.length - 1)]
                                .id,
                            userId: user.id,
                            isUpvote: Math.random() > 0.5,
                        },
                    );
                    votes.push(vote);
                }
            }
        }
        // for (const post of posts) {
        //     // Generate a random number of vote
        //     const numberOfVotes = getRandomBetween(5, 8);
        //     for (let i = 0; i < numberOfVotes; i++) {
        //         const vote = await this.votesService.createOrUpdatePostVote({
        //             postId: post.id,
        //             userId: users[getRandomBetween(0, users.length - 1)].id,
        //             isUpvote: Math.random() > 0.5,
        //         });
        //         votes.push(vote);
        //     }
        // }
        return votes;
    }

    async seedCommentVotes(
        users: User[],
        comments: Comment[],
    ): Promise<Vote[]> {
        const votes = [];
        for (const comment of comments) {
            // Generate a random number of vote
            const numberOfVotes = getRandomBetween(5, 10);
            for (let i = 0; i < numberOfVotes; i++) {
                const vote = await this.votesService.createOrUpdateCommentVote({
                    commentId: comment.id,
                    userId: users[getRandomBetween(0, users.length - 1)].id,
                    isUpvote: Math.random() > 0.5,
                });
                votes.push(vote);
            }
        }
        return votes;
    }
}
