import { User } from '../users/entities/user.entity';
import { Injectable } from '@nestjs/common';
import { Language } from '../code/entities/code.entity';
import { PostsService } from '../social-network/posts/posts.service';
import { getRandomPastDate } from '../helpers/date.helper';
import { Post } from '../social-network/posts/entities/post.entity';

@Injectable()
export class PostsSeeder {
    constructor(private readonly postsService: PostsService) {}

    async seed(users: User[]): Promise<Post[]> {
        const posts = [];

        const post1 = await this.postsService.create({
            authorId: users[0].id,
            title: `Addition`,
            content: `This code allows you to add two numbers.`,
            codes: [
                {
                    title: 'Addition',
                    language: Language.PYTHON,
                    code: `fun addition(a, b) {\n    return a + b;\n}\nprint('1 + 2 should equal 3 -> ', addition(1, 2));`,
                },
            ],
        });

        const post2 = await this.postsService.create({
            title: `Merge Two Lists Into a Dictionary`,
            content: `Assume we have two lists in Python and we want to merge them in a dictionary form, where one list’s items act as the dictionary’s keys and the other’s as the values. This is a frequent problem often faced when writing code in Python.
                To solve this problem, we need to consider a couple of restrictions, such as the sizes of the two lists, the types of items in the two lists and if there are any repeated items in them, especially in the one we’ll use as keys. We can overcome that with the use of built-in functions like zip.`,
            authorId: users[0].id,
            codes: [
                {
                    title: `Merge Two Lists Into a Dictionary`,
                    language: Language.PYTHON,
                    code: `keys_list = ['A', 'B', 'C']\nvalues_list = ['blue', 'red', 'bold']\n\n#There are 3 ways to convert these two lists into a dictionary\n#1- Using Python's zip, dict functionz\ndict_method_1 = dict(zip(keys_list, values_list))\n\n#2- Using the zip function with dictionary comprehensions\ndict_method_2 = {key:value for key, value in zip(keys_list, values_list)}\n\n#3- Using the zip function with a loop\nitems_tuples = zip(keys_list, values_list) \ndict_method_3 = {} \nfor key, value in items_tuples: \n    if key in dict_method_3: \n        pass # To avoid repeating keys.\n    else: \n        dict_method_3[key] = value`,
                },
            ],
        });

        const post3 = await this.postsService.create({
            title: `Merge Two or More Lists Into a List of Lists`,
            content: `Another frequent task is when we have two or more lists, and we want to collect them all in one big list of lists, where all the first items of the smaller list form the first list in the bigger list.
                For example, if I have 4 lists [1,2,3], [‘a’,’b’,’c’], [‘h’,’e’,’y’] and [4,5,6] and we want to make a new list of those four lists; it will be [[1,’a’,’h’,4], [2,’b’,’e’,5], [3,’c’,’y’,6]].`,
            authorId: users[1].id,
            codes: [
                {
                    title: `Merge Two or More Lists Into a List of Lists`,
                    language: Language.PYTHON,
                    code: `def merge(*args, missing_val = None):\n    #missing_val will be used when one of the smaller lists is shorter tham the others.\n    #Get the maximum length within the smaller lists.\n    max_length = max([len(lst) for lst in args])\n    outList = []\n    for i in range(max_length):\n        result.append([args[k][i] if i < len(args[k]) else missing_val for k in range(len(args))])\n    return outList`,
                },
            ],
        });

        const post4 = await this.postsService.create({
            title: `Sort a List of Dictionaries`,
            content: `The next set of everyday list tasks is sorting. Depending on the data type of the items included in the lists, we’ll follow a slightly different way to sort them. Let’s first start with sorting a list of dictionaries.`,
            authorId: users[1].id,
            codes: [
                {
                    title: `Sort a List of Dictionaries`,
                    language: Language.PYTHON,
                    code: `dicts_lists = [\n    {\n        "Name": "James",\n        "Age": 20,\n    },\n    {\n        "Name": "May",\n        "Age": 14,\n    },\n    {\n        "Name": "Katy",\n        "Age": 23,\n    }\n]
                    \n#There are different ways to sort that list\n#1- Using the sort/ sorted function based on the age\ndicts_lists.sort(key=lambda item: item.get("Age"))\n\n#2- Using itemgetter module based on name\nfrom operator import itemgetter\nf = itemgetter('Name')\ndicts_lists.sort(key=f)`,
                },
            ],
        });

        const post5 = await this.postsService.create({
            title: `Sort a List of Strings`,
            content: `We’re often faced with lists containing strings, and we need to sort those lists alphabetically, by length or any other factor we want (or that our application needs).
                Now, I should mention that these are straightforward ways to sort a list of strings, but you may sometimes need to implement your sorting algorithm to solve that problem.`,
            authorId: users[1].id,
            codes: [
                {
                    title: `Sort a List of Strings`,
                    language: Language.PYTHON,
                    code: `my_list = ["blue", "red", "green"]\n\n#1- Using sort or srted directly or with specifc keys\nmy_list.sort() #sorts alphabetically or in an ascending order for numeric data \nmy_list = sorted(my_list, key=len) #sorts the list based on the length of the strings from shortest to longest. \n# You can use reverse=True to flip the order\n\n#2- Using locale and functools \nimport locale\nfrom functools import cmp_to_key\nmy_list = sorted(my_list, key=cmp_to_key(locale.strcoll))`,
                },
            ],
        });

        const post6 = await this.postsService.create({
            title: `Sort a List Based on Another List`,
            content: `Sometimes, we may want or need to use one list to sort another. So, we’ll have a list of numbers (the indexes) and a list that I want to sort using these indexes.`,
            authorId: users[2].id,
            codes: [
                {
                    title: `Sort a List Based on Another List`,
                    language: Language.PYTHON,
                    code: `a = ['blue', 'green', 'orange', 'purple', 'yellow']\nb = [3, 2, 5, 4, 1]\n#Use list comprehensions to sort these lists\nsortedList =  [val for (_, val) in sorted(zip(b, a), key=lambda x: \\ x[0])]`,
                },
            ],
        });

        const post7 = await this.postsService.create({
            title: `Map a List Into a Dictionary`,
            content: `The last list-related task we’ll look at in this article is if we’re given a list and map it into a dictionary. That is, I want to convert my list into a dictionary with numerical keys.`,
            authorId: users[2].id,
            codes: [
                {
                    title: `Map a List Into a Dictionary`,
                    language: Language.PYTHON,
                    code: `mylist = ['blue', 'orange', 'green']\n#Map the list into a dict using the map, zip and dict functions\nmapped_dict = dict(zip(itr, map(fn, itr)))`,
                },
            ],
        });

        const post8 = await this.postsService.create({
            title: `Filter a List Based on a Condition`,
            content: `The next set of everyday list tasks is filtering. Depending on the data type of the items included in the lists, we’ll follow a slightly different way to filter them. Let’s first start with filtering a list of dictionaries.`,
            authorId: users[3].id,
            codes: [
                {
                    title: `Filter a List Based on a Condition`,
                    language: Language.PYTHON,
                    code: `dicts_lists = [\n    {\n        "Name": "James",\n        "Age": 20,\n    },\n    {\n        "Name": "May",\n        "Age": 14,\n    },\n    {\n        "Name": "Katy",\n        "Age": 23,\n    }\n]
                    \n#There are different ways to sort that list\n#1- Using the filter function based on the age\ndicts_lists = list(filter(lambda item: item.get("Age") > 18, dicts_lists))\n\n#2- Using itemgetter module based on name\nfrom operator import itemgetter\nf = itemgetter('Name')\ndicts_lists = list(filter(lambda item: item.get("Name") == "James", dicts_lists))`,
                },
            ],
        });

        await this.postsService.update(post1.id, {
            createdAt: getRandomPastDate(),
        });
        await this.postsService.update(post2.id, {
            createdAt: getRandomPastDate(),
        });
        await this.postsService.update(post3.id, {
            createdAt: getRandomPastDate(),
        });
        await this.postsService.update(post4.id, {
            createdAt: getRandomPastDate(),
        });
        await this.postsService.update(post5.id, {
            createdAt: getRandomPastDate(),
        });
        await this.postsService.update(post6.id, {
            createdAt: getRandomPastDate(),
        });
        await this.postsService.update(post7.id, {
            createdAt: getRandomPastDate(),
        });
        await this.postsService.update(post8.id, {
            createdAt: getRandomPastDate(),
        });

        posts.push(post1, post2, post3, post4, post5, post6, post7, post8);
        return posts;
    }
}
