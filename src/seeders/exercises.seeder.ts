import { Injectable } from '@nestjs/common';
import {
    Difficulty,
    Exercise,
} from '../collaborative/exercises/entities/exercice.entity';
import { ExercisesService } from '../collaborative/exercises/exercises.service';
import { Language } from '../code/entities/code.entity';
import { ValidatingTestService } from '../collaborative/validating-test/validating-test.service';

@Injectable()
export class ExercisesSeeder {
    constructor(
        private readonly exercisesService: ExercisesService,
        private readonly validatingTestsService: ValidatingTestService,
    ) {}

    async seedExercises(): Promise<Exercise[]> {
        const exercises = [];

        let alreadyExists =
            await this.exercisesService.findByNameAndLanguageAndDifficulty(
                'Additionner',
                Language.PYTHON,
                Difficulty.NOOB,
            );

        if (!alreadyExists) {
            const exercise1 = await this.exercisesService.create({
                name: 'Additionner',
                instructions:
                    "Écrire une fonction qui affiche la somme de deux nombres entiers sous la forme : '2 + 2 = 4'.",
                language: Language.PYTHON,
                difficulty: Difficulty.NOOB,
                defaultCode: 'def sumUp(a, b):\n    ',
                memoryLimit: 2048,
            });
            exercises.push(exercise1);

            this.validatingTestsService.create({
                name: 'Additionner 2 nombres positifs',
                description: '',
                exerciseId: exercise1.id,
                difficulty: Difficulty.NOOB,
                codeToExecute: 'sumUp(2, 2)',
                inputParameters: '2, 2',
                expectedOutput: '4',
            });

            this.validatingTestsService.create({
                name: 'Additionner 2 nombres négatifs',
                description: '',
                exerciseId: exercise1.id,
                difficulty: Difficulty.EASY,
                codeToExecute: 'sumUp(-123, -39)',
                inputParameters: '-123, -39',
                expectedOutput: '-162',
            });

            this.validatingTestsService.create({
                name: 'Additionner 1 nombre positif et 1 nombres négatif',
                description: '',
                exerciseId: exercise1.id,
                difficulty: Difficulty.MEDIUM,
                codeToExecute: 'sumUp(591, -1284)',
                inputParameters: '591, -1284',
                expectedOutput: '-693',
            });
        }

        //

        alreadyExists =
            await this.exercisesService.findByNameAndLanguageAndDifficulty(
                "Compter jusqu'à 10",
                Language.PYTHON,
                Difficulty.NOOB,
            );

        if (!alreadyExists) {
            const exercise2 = await this.exercisesService.create({
                name: "Compter jusqu'à 10",
                instructions:
                    'Écrire une fonction qui affiche un tableau de 10 nombres de 1 à 10 espacés par un espace.',
                language: Language.PYTHON,
                difficulty: Difficulty.NOOB,
                defaultCode: 'def count_to_ten():\n    ',
                memoryLimit: 2048,
            });
            exercises.push(exercise2);
        }

        alreadyExists =
            await this.exercisesService.findByNameAndLanguageAndDifficulty(
                'Factorielle',
                Language.PYTHON,
                Difficulty.EASY,
            );

        if (!alreadyExists) {
            const exercise3 = await this.exercisesService.create({
                name: 'Factorielle',
                instructions:
                    "Écrire une fonction qui affiche la factorielle d'un nombre entier (si n = 3, alors 1 * 2 * 3 = 6)",
                language: Language.PYTHON,
                difficulty: Difficulty.EASY,
                defaultCode: 'def factorial(n):\n    ',
                memoryLimit: 2048,
            });

            exercises.push(exercise3);
        }

        alreadyExists =
            await this.exercisesService.findByNameAndLanguageAndDifficulty(
                'Décompter',
                Language.PYTHON,
                Difficulty.MEDIUM,
            );

        if (!alreadyExists) {
            const exercise4 = await this.exercisesService.create({
                name: 'Décompter',
                instructions:
                    'Écrire une fonction qui affiche un tableau de nombres de n à 0, espacés par un espace.',
                language: Language.PYTHON,
                difficulty: Difficulty.MEDIUM,
                defaultCode: 'def decount_to(n):\n    ',
                memoryLimit: 2048,
            });
            exercises.push(exercise4);

            this.validatingTestsService.create({
                name: 'Décompter 10',
                description: '',
                exerciseId: exercise4.id,
                difficulty: Difficulty.MEDIUM,
                codeToExecute: 'decount_to(10)',
                inputParameters: '10',
                expectedOutput: '10 9 8 7 6 5 4 3 2 1 0',
            });

            this.validatingTestsService.create({
                name: 'Décompter 0',
                description: '',
                exerciseId: exercise4.id,
                difficulty: Difficulty.MEDIUM,
                codeToExecute: 'decount_to(0)',
                inputParameters: '0',
                expectedOutput: '0',
            });

            this.validatingTestsService.create({
                name: 'Décompter -10',
                description: '',
                exerciseId: exercise4.id,
                difficulty: Difficulty.MEDIUM,
                codeToExecute: 'decount_to(-10)',
                inputParameters: '-10',
                expectedOutput: '-10 -9 -8 -7 -6 -5 -4 -3 -2 -1 0',
            });
        }

        return exercises;
    }
}
