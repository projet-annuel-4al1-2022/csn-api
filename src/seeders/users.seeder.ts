import { UsersService } from '../users/users.service';
import { User } from '../users/entities/user.entity';
import { Injectable } from '@nestjs/common';
import faker from '@faker-js/faker';

@Injectable()
export class UsersSeeder {
    constructor(private readonly usersService: UsersService) {}

    async seed(): Promise<User[]> {
        const users = [];

        const user1 = await this.usersService.createUser({
            email: 'william.quach@outlook.fr',
            biography: 'I am a back-end developer',
            firstName: 'William',
            lastName: 'QUACH',
            password: 'azeaze',
            pseudo: 'wquach',
        });

        const user2 = await this.usersService.createUser({
            email: 'soniasimo11@gmail.com',
            biography: 'I am an fullstack developer',
            firstName: 'sonia',
            lastName: 'SIMOHAMMED',
            password: 'azeaze',
            pseudo: 'ssimohammed',
        });
        const user3 = await this.usersService.createUser({
            email: 'luigi.carole@gmail.com',
            biography: 'I am an front-end developer',
            firstName: 'Luigi',
            lastName: 'CAROLE',
            password: 'azeaze',
            pseudo: 'lcarole',
        });

        const user0 = await this.usersService.createUser({
            email: 'user-test@gmail.com',
            biography: "Hello I'm user 0",
            firstName: 'User',
            lastName: 'Zero',
            password: 'azeaze',
            pseudo: 'userZero',
        });

        const user4 = await this.usersService.createUser({
            email: faker.internet.email(),
            biography: faker.lorem.sentence(),
            firstName: faker.name.firstName(),
            lastName: faker.name.lastName(),
            password: 'azeaze',
            pseudo: faker.internet.userName(),
        });

        const user5 = await this.usersService.createUser({
            email: faker.internet.email(),
            biography: faker.lorem.sentence(),
            firstName: faker.name.firstName(),
            lastName: faker.name.lastName(),
            password: 'azeaze',
            pseudo: faker.internet.userName(),
        });

        const user6 = await this.usersService.createUser({
            email: faker.internet.email(),
            biography: faker.lorem.sentence(),
            firstName: faker.name.firstName(),
            lastName: faker.name.lastName(),
            password: 'azeaze',
            pseudo: faker.internet.userName(),
        });

        const user7 = await this.usersService.createUser({
            email: faker.internet.email(),
            biography: faker.lorem.sentence(),
            firstName: faker.name.firstName(),
            lastName: faker.name.lastName(),
            password: 'azeaze',
            pseudo: faker.internet.userName(),
        });

        const user8 = await this.usersService.createUser({
            email: faker.internet.email(),
            biography: faker.lorem.sentence(),
            firstName: faker.name.firstName(),
            lastName: faker.name.lastName(),
            password: 'azeaze',
            pseudo: faker.internet.userName(),
        });

        users.push(user0);
        users.push(user1);
        users.push(user2);
        users.push(user3);
        users.push(user4);
        users.push(user5);
        users.push(user6);
        users.push(user7);
        users.push(user8);

        user1.following = [user0];
        user1.following = [user1];
        user1.following = [user2];
        user1.following = [user3];
        user1.following = [user4];

        user2.following = [user1];
        user2.following = [user3];
        user2.following = [user4];
        user2.following = [user0];

        user3.following = [user0];
        user3.following = [user2];
        user3.following = [user4];
        user3.following = [user5];
        user3.following = [user7];

        user0.following = [user1];
        user0.following = [user2];
        user0.following = [user3];
        user0.following = [user4];
        user0.following = [user5];
        user0.following = [user6];
        user0.following = [user7];
        user0.following = [user8];

        await this.usersService.save(user1);
        await this.usersService.save(user2);
        await this.usersService.save(user3);
        await this.usersService.save(user0);
        await this.usersService.save(user4);
        await this.usersService.save(user5);
        await this.usersService.save(user6);
        await this.usersService.save(user7);
        await this.usersService.save(user8);

        return users;
    }
}
