import { Injectable } from '@nestjs/common';
import { UsersSeeder } from './users.seeder';
import { PostsSeeder } from './posts.seeder';
import { ViewsSeeder } from './views.seeder';
import { VotesSeeder } from './votes.seeder';
import { CommentsSeeder } from './comments.seeder';
import { ExercisesSeeder } from './exercises.seeder';

@Injectable()
export class GlobalSeeder {
    constructor(
        private readonly usersSeeder: UsersSeeder,
        private readonly postsSeeder: PostsSeeder,
        private readonly viewsSeeder: ViewsSeeder,
        private readonly votesSeeder: VotesSeeder,
        private readonly commentsSeeder: CommentsSeeder,
        private readonly exercisesSeeder: ExercisesSeeder,
    ) {
        this.seed();
    }

    async seed() {
        console.log('Seeding...');
        try {
            const exercises = await this.exercisesSeeder.seedExercises();
            console.log(
                'Seeded ' + exercises.length + ' exercises successfully',
            );
            const users = await this.usersSeeder.seed();
            console.log('Seeded ' + users.length + ' users successfully');
            const posts = await this.postsSeeder.seed(users);
            console.log('Seeded ' + posts.length + ' posts successfully');
            const views = await this.viewsSeeder.seed(users, posts);
            console.log('Seeded ' + views.length + ' views successfully');
            const postVotes = await this.votesSeeder.seedPostVotes(
                users,
                posts,
            );
            console.log(
                'Seeded ' + postVotes.length + ' post votes successfully',
            );
            const comments = await this.commentsSeeder.seed(users, posts);
            console.log('Seeded ' + comments.length + ' comments successfully');
            // const commentVotes = await this.votesSeeder.seedCommentVotes(
            //     users,
            //     comments,
            // );
            // console.log(
            //     'Seeded ' + commentVotes.length + ' comment votes successfully',
            // );
            console.log('Seeded successfully');
        } catch (error) {
            console.error('/!\\');
            console.error('/!\\');
            console.error('Failed when seeding !!!!');
            // console.error(JSON.stringify(error, null, 4));
            console.error('/!\\');
            console.error('/!\\');
        }
    }
}
