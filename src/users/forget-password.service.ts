import {
    BadRequestException,
    HttpException,
    HttpStatus,
    Injectable,
} from '@nestjs/common';
import { User } from './entities/user.entity';
import { config } from 'dotenv';
import { MailService } from '../mail/mail.service';
import * as crypto from 'crypto';
import { UsersService } from './users.service';
import * as moment from 'moment';

config();

@Injectable()
export class ForgetPasswordService {
    constructor(
        private readonly usersService: UsersService,
        private readonly mailService: MailService,
    ) {}

    async forgotPassword(userEmail: string) {
        const user = await this.usersService.findByEmail(userEmail);
        if (!user) {
            throw new HttpException(
                {
                    status: HttpStatus.NOT_FOUND,
                    error: `User not found with email = '${userEmail}'`,
                },
                HttpStatus.NOT_FOUND,
            );
        }
        const token = ForgetPasswordService.generateResetToken(user);

        return await this.mailService.sendUserResetPasswordMail(user, token);
    }

    private static generateResetToken(user: User): string {
        // Date now - will use it for expiration
        const now = new Date();

        // Convert to Base64
        const timeBase64 = Buffer.from(now.toISOString()).toString('base64');

        //Convert to Base64 user UUID - will use for retrieve user
        const userIDBase64 = Buffer.from(String(user.id)).toString('base64');

        // User info string - will use it for sign and use token once
        const userString = `${user.id}${user.email}${user.password}`;
        const userStringHash = crypto
            .createHash('md5')
            .update(userString)
            .digest('hex');

        // Generate a formatted string [time]-[userSign]-[userUUID]
        const tokenize = `${timeBase64}-${userStringHash}-${userIDBase64}`;

        // encrypt token
        return ForgetPasswordService.encryptToken(tokenize);
    }

    // Encrypt token with password using crypto.Cipheriv
    private static encryptToken(stringToEncrypt: string): string {
        const key = crypto.createHash('sha256').update('popcorn').digest();

        const IV_LENGTH = 16;
        const iv = crypto.randomBytes(IV_LENGTH);
        const cipher = crypto.createCipheriv('aes-256-cbc', key, iv);
        const encrypted = cipher.update(stringToEncrypt);

        const result = Buffer.concat([encrypted, cipher.final()]);

        // formatted string [iv]:[token]
        return iv.toString('hex') + ':' + result.toString('hex');
    }

    async resetPassword(token: string, newPassword: string): Promise<User> {
        // Decrypt token
        const decryptedToken = ForgetPasswordService.decryptToken(token);

        if (!decryptedToken) {
            throw new BadRequestException('Invalid token');
        }

        //Extract userId from decrypted token - will use it for validate token also
        const userId = ForgetPasswordService.getUserIDFromToken(decryptedToken);

        if (!userId) {
            throw new BadRequestException('Invalid token');
        }

        const user = await this.usersService.findById(parseInt(userId));

        if (!user) {
            throw new BadRequestException('Invalid token');
        }

        // Validate Token - expiration and unicity
        const isTokenValid = await ForgetPasswordService.validateResetToken(
            user,
            decryptedToken,
        );

        if (!isTokenValid) {
            throw new BadRequestException('Invalid token');
        }

        // Update user password
        await this.usersService.update(user.id, {
            password: await this.usersService.hashPassword(newPassword),
        });

        return user;
    }

    // Decrypt token using the inverse of encryption crypto algorithm
    private static decryptToken(stringToDecrypt: string): string {
        try {
            const key = crypto.createHash('sha256').update('popcorn').digest();

            const textParts = stringToDecrypt.split(':');
            const iv = Buffer.from(textParts.shift(), 'hex');
            const encryptedText = Buffer.from(textParts.join(':'), 'hex');
            const decipher = crypto.createDecipheriv('aes-256-cbc', key, iv);
            const decrypted = decipher.update(encryptedText);

            const result = Buffer.concat([decrypted, decipher.final()]);

            return result.toString();
        } catch (error) {
            console.log('decrypted token error', error);
            return null;
        }
    }

    // Extract userUUID from decrypted token string
    private static getUserIDFromToken(token: string): string {
        try {
            const userIDHash = token.split('-')[2];
            return Buffer.from(userIDHash, 'base64').toString('ascii');
        } catch (error) {
            console.log('getUserIDFromToken', error);
            return null;
        }
    }

    // Validate the token
    private static async validateResetToken(
        user: User,
        token: string,
    ): Promise<boolean> {
        // Split token string and retrieve timeInfo and userInfoHash
        const [timeHBase64, reqUserStringHash] = token.split('-');

        const timestamp = Buffer.from(timeHBase64, 'base64').toString('ascii');

        // Using moment.diff method for retrieve dates difference in hours
        const tokenTimestampDate = moment(timestamp);
        const now = moment();

        // Fail if more then 24 hours
        const diff = now.diff(tokenTimestampDate, 'hours');
        if (Math.abs(diff) > 24) return false;

        const userString = `${user.id}${user.email}${user.password}`;
        const userStringHash = crypto
            .createHash('md5')
            .update(userString)
            .digest('hex');

        // Check if userInfoHash is the same - this guarantee the token used once
        return reqUserStringHash === userStringHash;
    }
}
