import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from './entities/user.entity';
import { Repository } from 'typeorm';
import { CreateUserDto } from './DTO/create-user.dto';
import { config } from 'dotenv';
import * as bcrypt from 'bcrypt';
import { UpdateUserDto } from './DTO/update-user.dto';

config();

@Injectable()
export class UsersService {
    constructor(
        @InjectRepository(User)
        private readonly usersRepository: Repository<User>,
    ) {}

    async createUser(user: CreateUserDto): Promise<User> {
        if (user.password === '') {
            throw new Error('User password cannot be empty');
        }
        user.password = await this.hashPassword(user.password);
        const saved = await this.usersRepository.save({
            ...user,
            followers: [],
            posts: [],
        });
        return this.usersRepository.findOne(saved.id);
    }

    public async hashPassword(password: string) {
        const salt = await bcrypt.genSalt(
            parseInt(process.env.PASSWORD_ENCRYPTION_SALT),
        );
        return bcrypt.hash(password, salt);
    }

    async update(userId: number, userData: Partial<UpdateUserDto>) {
        const toUpdate = await this.usersRepository.findOneOrFail(userId);
        userData.email = userData.email ?? toUpdate.email;
        userData.pseudo = userData.pseudo ?? toUpdate.pseudo;
        userData.biography = userData.biography ?? toUpdate.biography;
        userData.firstName = userData.firstName ?? toUpdate.firstName;
        userData.lastName = userData.lastName ?? toUpdate.lastName;
        userData.password = userData.password
            ? await this.hashPassword(userData.password)
            : toUpdate.password;
        const saved = await this.usersRepository.save({
            ...toUpdate,
            ...userData,
        });
        return this.usersRepository.findOne(saved.id);
    }

    async findAll(): Promise<User[]> {
        return this.usersRepository.find({
            relations: ['posts', 'followers', 'following'],
        });
    }

    async findById(userId: number): Promise<User> {
        return this.usersRepository
            .createQueryBuilder('user')
            .where('user.id = :userId', { userId })
            .leftJoinAndSelect('user.posts', 'posts')
            .leftJoinAndSelect('user.followers', 'followers')
            .leftJoinAndSelect('user.following', 'following')
            .leftJoinAndSelect(
                'following.challengeInfos',
                'followingChallengeInfos',
            )
            .leftJoinAndSelect('following.followers', 'followingFollowers')
            .leftJoinAndSelect('following.following', 'followingFollowing')
            .leftJoinAndSelect('followers.followers', 'followersFollowers')
            .leftJoinAndSelect('followers.following', 'followersFollowing')
            .leftJoinAndSelect('following.posts', 'followingPosts')
            .leftJoinAndSelect('followers.posts', 'followersPosts')
            .leftJoinAndSelect('user.challengeInfos', 'challengeInfos')
            .getOneOrFail();
    }

    async findByEmail(userEmail: string): Promise<User> {
        return this.usersRepository
            .createQueryBuilder('user')
            .where('user.email = :userEmail', { userEmail })
            .leftJoinAndSelect('user.posts', 'posts')
            .leftJoinAndSelect('user.followers', 'followers')
            .leftJoinAndSelect('user.following', 'following')
            .leftJoinAndSelect('following.followers', 'followingFollowers')
            .leftJoinAndSelect('following.following', 'followingFollowing')
            .leftJoinAndSelect('followers.followers', 'followersFollowers')
            .leftJoinAndSelect('followers.following', 'followersFollowing')
            .leftJoinAndSelect('following.posts', 'followingPosts')
            .leftJoinAndSelect('followers.posts', 'followersPosts')
            .getOneOrFail();
    }

    async findByPseudo(pseudo: string): Promise<User> {
        return this.usersRepository
            .createQueryBuilder('user')
            .where('user.pseudo = :pseudo', { pseudo })
            .leftJoinAndSelect('user.posts', 'posts')
            .leftJoinAndSelect('user.followers', 'followers')
            .leftJoinAndSelect('user.following', 'following')
            .leftJoinAndSelect('following.followers', 'followingFollowers')
            .leftJoinAndSelect('following.following', 'followingFollowing')
            .leftJoinAndSelect('followers.followers', 'followersFollowers')
            .leftJoinAndSelect('followers.following', 'followersFollowing')
            .leftJoinAndSelect('following.posts', 'followingPosts')
            .leftJoinAndSelect('followers.posts', 'followersPosts')
            .getOneOrFail();
    }

    async save(user: User): Promise<User> {
        const userSaved = await this.usersRepository.save(user);
        return this.findById(userSaved.id);
    }

    async getMutuals(userId: number) {
        const user = await this.findById(userId);
        return user.following.filter(
            (following) =>
                user.followers.find(
                    (follower) => follower.id === following.id,
                ) !== undefined,
        );
    }

    async search(pseudo: string, limit: number) {
        console.log('Searching for: ', pseudo);
        // Using SOUNDEX to search for a pseudo in mysql user database
        return await this.usersRepository
            .createQueryBuilder('user')
            // Using fuzzy search with nestJS
            .where('user.pseudo LIKE :pseudo', {
                pseudo: `%${pseudo}%`,
            })
            .take(limit)
            .getMany();
    }
}
