import { UserIdDto } from './user-id.dto';
import { PostIdDto } from '../../social-network/posts/dto/post-id.dto';

export class GetUserDto {
    public id: number;
    public firstName: string;
    public lastName: string;
    public fullName: string;

    public pseudo: string;
    public biography: string;

    public email: string;

    public followers: UserIdDto[];
    public following: UserIdDto[];

    public followersCount: number;
    public followingCount: number;

    public posts: PostIdDto[];
    public postsCount: number;

    public isUserFollowedByUserRequesting: boolean;

    public createdAt: Date;
    public updatedAt: Date;
    public deletedAt: Date;
}
