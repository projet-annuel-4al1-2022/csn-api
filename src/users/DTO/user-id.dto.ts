export class UserIdDto {
    public userId: number;

    constructor(userId: number) {
        this.userId = userId;
    }
}
