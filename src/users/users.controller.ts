import {
    Body,
    Controller,
    Get,
    HttpException,
    HttpStatus,
    Param,
    Patch,
    Post,
    Query,
} from '@nestjs/common';
import { UsersService } from './users.service';
import { ForgetPasswordService } from './forget-password.service';
import { GetUserDto } from './DTO/get-user.dto';
import { GetPostDto } from '../social-network/posts/dto/get-post.dto';
import { PostsService } from '../social-network/posts/posts.service';
import * as moment from 'moment';
import { UpdateUserDto } from './DTO/update-user.dto';
import { User } from './entities/user.entity';
import { GetSnippetDto } from '../code/snippets/dto/get-snippet.dto';
import { SnippetsService } from '../code/snippets/snippets.service';

@Controller('users')
export class UsersController {
    constructor(
        private readonly usersService: UsersService,
        private readonly postsService: PostsService,
        private readonly forgotPasswordService: ForgetPasswordService,
        private readonly snippetsService: SnippetsService,
    ) {}

    // Search user by pseudo
    @Get('/search')
    async search(@Query('pseudo') pseudo: string): Promise<GetUserDto[]> {
        return (await this.usersService.search(pseudo, 10)).map((user) =>
            user.toDto(),
        );
    }

    @Patch('/:id')
    async updateUser(
        @Param('id') id: number,
        @Body() body: UpdateUserDto,
        @Query('userRequestingId') userRequestingId?: number,
    ): Promise<GetUserDto> {
        return (await this.usersService.update(id, body)).toDto(
            userRequestingId,
        );
    }

    @Get()
    async findAll(): Promise<GetUserDto[]> {
        return (await this.usersService.findAll()).map((user) => user.toDto());
    }

    @Get('/:userIdOrEmailOrPseudo')
    async findUser(
        @Param() params,
        @Query('userRequestingId') userRequestingId?: number,
    ): Promise<GetUserDto> {
        let userById: User;
        let userByEmail: User;
        let userByPseudo: User;
        try {
            userById = parseInt(params.userIdOrEmailOrPseudo)
                ? await this.usersService.findById(
                      parseInt(params.userIdOrEmailOrPseudo),
                  )
                : undefined;
        } catch (e) {}
        try {
            userByEmail = await this.usersService.findByEmail(
                params.userIdOrEmailOrPseudo,
            );
        } catch (e) {}
        try {
            userByPseudo = await this.usersService.findByPseudo(
                params.userIdOrEmailOrPseudo,
            );
        } catch (e) {}

        if (!userById && !userByEmail && !userByPseudo) {
            throw new HttpException(
                {
                    status: HttpStatus.NOT_FOUND,
                    error: `User not found with id or email or pseudo = '${params.userIdOrEmailOrPseudo}'`,
                },
                HttpStatus.NOT_FOUND,
            );
        }
        return (
            userById?.toDto(userRequestingId) ||
            userByEmail?.toDto(userRequestingId) ||
            userByPseudo.toDto(userRequestingId)
        );
    }

    @Get(':userEmail/forgot-password')
    async forgotPassword(@Param() params): Promise<any> {
        return this.forgotPasswordService.forgotPassword(
            String(params.userEmail),
        );
    }

    @Patch('/reset-password/:token')
    async resetPassword(
        @Param() params,
        @Body() body: { newPassword: string },
    ): Promise<GetUserDto> {
        if (!body.newPassword) {
            throw new HttpException(
                {
                    status: HttpStatus.BAD_REQUEST,
                    error: `No new password given`,
                },
                HttpStatus.BAD_REQUEST,
            );
        }
        return (
            await this.forgotPasswordService.resetPassword(
                params.token,
                body.newPassword,
            )
        ).toDto();
    }

    @Post('/:userId/follow/:userIdToFollow')
    async followUser(
        @Param('userId') userId: number,
        @Param('userIdToFollow') userIdToFollow: number,
    ): Promise<GetUserDto> {
        const user = await this.usersService.findById(userId);
        const userToFollow = await this.usersService.findById(userIdToFollow);
        if (!user || !userToFollow) {
            const userIdNotFound = user ? userIdToFollow : userId;
            throw new HttpException(
                {
                    status: HttpStatus.NOT_FOUND,
                    error: `User not found with id = '${userIdNotFound} when user with id '${userId}' tried to follow user with id = '${userIdToFollow}'`,
                },
                HttpStatus.NOT_FOUND,
            );
        }
        user.following.push(userToFollow);
        return (await this.usersService.save(user)).toDto();
    }

    @Post('/:userId/unfollow/:userIdToUnfollow')
    async unfollowUser(
        @Param('userId') userId: number,
        @Param('userIdToUnfollow') userIdToUnfollow: number,
    ): Promise<GetUserDto> {
        const user = await this.usersService.findById(userId);
        const userToUnfollow = await this.usersService.findById(
            userIdToUnfollow,
        );
        if (!user || !userToUnfollow) {
            const userIdNotFound = user ? userIdToUnfollow : userId;
            throw new HttpException(
                {
                    status: HttpStatus.NOT_FOUND,
                    error: `User not found with id = '${userIdNotFound} when user with id '${userId}' tried to unfollow user with id = '${userIdToUnfollow}'`,
                },
                HttpStatus.NOT_FOUND,
            );
        }
        const userToUnfollowIndex = user.following
            .map((following) => following.id)
            .indexOf(userToUnfollow.id, 0);
        if (userToUnfollowIndex > -1) {
            user.following.splice(userToUnfollowIndex, 1);
        }
        return (await this.usersService.save(user)).toDto();
    }

    @Get('/:userId/following')
    async getUserFollowing(
        @Param('userId') userId: number,
    ): Promise<GetUserDto[]> {
        const user = await this.usersService.findById(userId);
        if (!user) {
            throw new HttpException(
                {
                    status: HttpStatus.NOT_FOUND,
                    error: `User not found with id = '${userId} when trying to retrieve user's following`,
                },
                HttpStatus.NOT_FOUND,
            );
        }
        return user.following
            .map((following) => following.toDto())
            .sort((a, b) => b.followersCount - a.followersCount);
    }

    @Get('/:userId/followers')
    async getUserFollowers(
        @Param('userId') userId: number,
    ): Promise<GetUserDto[]> {
        const user = await this.usersService.findById(userId);
        if (!user) {
            throw new HttpException(
                {
                    status: HttpStatus.NOT_FOUND,
                    error: `User not found with id = '${userId} when trying to retrieve user's followers`,
                },
                HttpStatus.NOT_FOUND,
            );
        }
        return user.followers
            .map((follower) => follower.toDto())
            .sort((a, b) => b.followersCount - a.followersCount);
    }

    /**
     * Get's User feed (all posts from users the user is following)
     */
    @Get('/:userId/feed')
    async getUserFeed(
        @Param('userId') userId: number,
        @Query('maxDate') maxDate: string,
        @Query('limit') limit: number,
        @Query('userRequestingId') userRequestingId?: number,
    ): Promise<GetPostDto[]> {
        if (!maxDate) {
            throw new HttpException(
                {
                    status: HttpStatus.BAD_REQUEST,
                    error: `'maxDate' is required in query params when trying to retrieve user's feed`,
                },
                HttpStatus.BAD_REQUEST,
            );
        }
        const user = await this.usersService.findById(userId);
        if (!user) {
            throw new HttpException(
                {
                    status: HttpStatus.NOT_FOUND,
                    error: `User not found with id = '${userId} when trying to retrieve user's feed`,
                },
                HttpStatus.NOT_FOUND,
            );
        }
        const maxDateMoment = moment.utc(maxDate);
        if (!maxDateMoment.isValid()) {
            throw new HttpException(
                {
                    status: HttpStatus.BAD_REQUEST,
                    error: `Invalid maxDate '${maxDate}' when trying to retrieve user's feed`,
                },
                HttpStatus.BAD_REQUEST,
            );
        }
        if (maxDateMoment.isAfter(moment.utc())) {
            throw new HttpException(
                {
                    status: HttpStatus.BAD_REQUEST,
                    error: `Date cannot be in future '${maxDate}' when trying to retrieve user's feed`,
                },
                HttpStatus.BAD_REQUEST,
            );
        }
        if (!user.following || user.following.length === 0) {
            return [];
        }

        const userFeed = await this.postsService.getUserFeed(
            user.following.map((following) => following.id),
            maxDateMoment.toDate(),
            limit ?? 50,
        );
        return this.postsService
            .sortUsersFeed(userFeed)
            .map((post) => post.toDto(userRequestingId));
    }

    @Get('/:userId/trends')
    async trends(@Param('userId') userId: number): Promise<GetPostDto[]> {
        return (await this.postsService.trends(userId)).map((post) =>
            post.toDto(),
        );
    }

    @Get('/:userId/posts')
    async userPosts(
        @Param('userId') userId: number,
        @Query('userRequestingId') userRequestingId,
    ): Promise<GetPostDto[]> {
        return (await this.postsService.getUserPosts(userId)).map((post) =>
            post.toDto(userRequestingId),
        );
    }

    // Get snippets
    @Get('/:userId/snippets')
    async userSnippets(
        @Param('userId') userId: number,
    ): Promise<GetSnippetDto[]> {
        return (await this.snippetsService.findByUserId(userId)).map(
            (snippet) => snippet.toDto(),
        );
    }

    // Get users mutuals (person that follows him and he follows them)
    @Get('/:userId/mutuals')
    async userMutuals(@Param('userId') userId: number): Promise<GetUserDto[]> {
        return (await this.usersService.getMutuals(userId)).map((user) =>
            user.toDto(),
        );
    }
}
