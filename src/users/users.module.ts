import { forwardRef, Module } from '@nestjs/common';
import { UsersController } from './users.controller';
import { UsersService } from './users.service';
import { User } from './entities/user.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MailService } from '../mail/mail.service';
import { ForgetPasswordService } from './forget-password.service';
import { PostsModule } from '../social-network/posts/posts.module';
import { SnippetsModule } from '../code/snippets/snippets.module';

@Module({
    controllers: [UsersController],
    providers: [UsersService, ForgetPasswordService, MailService],
    exports: [UsersService],
    imports: [
        TypeOrmModule.forFeature([User]),
        forwardRef(() => PostsModule),
        forwardRef(() => SnippetsModule),
    ],
})
export class UsersModule {}
