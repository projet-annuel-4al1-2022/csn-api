import {
    Column,
    CreateDateColumn,
    DeleteDateColumn,
    Entity,
    JoinTable,
    ManyToMany,
    OneToMany,
    PrimaryGeneratedColumn,
    Unique,
    UpdateDateColumn,
} from 'typeorm';
import { Post } from '../../social-network/posts/entities/post.entity';
import { GetUserDto } from '../DTO/get-user.dto';
import { View } from '../../social-network/views/entities/view.entity';
import { UserIdDto } from '../DTO/user-id.dto';
import { PostIdDto } from '../../social-network/posts/dto/post-id.dto';
import { Vote } from '../../social-network/votes/entities/vote.entity';
import { Snippet } from '../../code/snippets/entities/snippet.entity';
import { Comment } from '../../social-network/comments/entities/comment.entity';
import { ChallengeInfo } from '../../collaborative/challenges/entities/challenger-info.entity';

@Entity({ name: 'users' })
@Unique('UNIQUE_EMAIL', ['email'])
@Unique('UNIQUE_PSEUDO', ['pseudo'])
export class User {
    @PrimaryGeneratedColumn('increment') id: number;
    @Column({ name: 'first_name' })
    public firstName: string;

    @Column({ name: 'last_name', nullable: true })
    public lastName: string;

    fullName(): string {
        return `${this.firstName}${this.lastName ? ' ' + this.lastName : ''}`;
    }

    @Column({ name: 'pseudo' })
    public pseudo: string;

    @Column({ name: 'biography', default: '' })
    public biography: string;

    @Column({ name: 'email' })
    public email: string;

    @Column({ name: 'password' })
    public password: string;

    @ManyToMany(() => User, (user) => user.following, {
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
    })
    @JoinTable({
        name: 'followers',
        joinColumn: {
            name: 'follower_id',
            referencedColumnName: 'id',
        },
        inverseJoinColumn: {
            name: 'following_id',
            referencedColumnName: 'id',
        },
    })
    public followers: User[];
    @ManyToMany(() => User, (user) => user.followers, {
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
    })
    public following: User[];

    public followersCount(): number {
        return this.followers?.length || 0;
    }

    public followingCount(): number {
        return this.following?.length || 0;
    }

    @OneToMany(() => Post, (post: Post) => post.author)
    public posts: Post[];

    public postsCount(): number {
        return this.posts?.length || 0;
    }

    @OneToMany(() => View, (view: View) => view.viewer)
    public views: View[];

    @OneToMany(() => Vote, (vote: Vote) => vote.user, {
        cascade: true,
    })
    public votes: Vote[];

    @OneToMany(() => Snippet, (snippet: Snippet) => snippet.user, {
        cascade: false,
    })
    public snippets: Snippet[];

    @OneToMany(() => Comment, (comment) => comment.author, {
        cascade: false,
    })
    public comments: Comment[];

    @OneToMany(() => ChallengeInfo, (challengeInfo) => challengeInfo.user, {
        cascade: true,
    })
    public challengeInfos: ChallengeInfo[];

    @CreateDateColumn({
        type: 'timestamptz',
        name: 'created_at',
    })
    public createdAt: Date;

    @UpdateDateColumn({
        type: 'timestamptz',
        name: 'updated_at',
    })
    public updatedAt: Date;

    @DeleteDateColumn({
        type: 'timestamptz',
        name: 'deleted_at',
        nullable: true,
    })
    public deletedAt: Date;

    public toDto(userRequestingId?: number): GetUserDto {
        return {
            id: this.id,
            firstName: this.firstName,
            lastName: this.lastName,
            fullName: `${this.fullName()}`,

            pseudo: this.pseudo,
            biography: this.biography,

            email: this.email,

            followers: this.followers
                ? this.following.map((follower) => new UserIdDto(follower.id))
                : [],
            following: this.following
                ? this.following.map((following) => new UserIdDto(following.id))
                : [],

            followersCount: this.followersCount(),
            followingCount: this.followingCount(),

            posts: this.posts
                ? this.posts.map((post) => new PostIdDto(post.id))
                : [],
            postsCount: this.postsCount(),

            isUserFollowedByUserRequesting:
                this.isUserFollowedByUserRequesting(userRequestingId),

            createdAt: this.createdAt,
            updatedAt: this.updatedAt,
            deletedAt: this.deletedAt,
        };
    }

    private isUserFollowedByUserRequesting(userRequestingId: number) {
        return this.followers?.some(
            (follower) => follower.id == userRequestingId,
        );
    }
}
