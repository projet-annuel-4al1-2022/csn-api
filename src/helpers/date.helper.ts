import * as moment from 'moment';
import { getRandomBetween } from './math.helper';

export const getRandomPastDate = (parameters?: {
    subtractDays?: number;
    subtractHours?: number;
    subtractMinutes?: number;
}): Date => {
    return moment()
        .subtract(getRandomBetween(30, parameters?.subtractDays ?? 62), 'days')
        .subtract(getRandomBetween(0, parameters?.subtractHours ?? 24), 'hours')
        .subtract(
            getRandomBetween(1, parameters?.subtractMinutes ?? 60),
            'minutes',
        )
        .toDate();
};
