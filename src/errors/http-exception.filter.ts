import {
    ArgumentsHost,
    Catch,
    ExceptionFilter,
    HttpException,
} from '@nestjs/common';
import { Request, Response } from 'express';

@Catch(HttpException)
export class HttpExceptionFilter implements ExceptionFilter {
    catch(exception: HttpException, host: ArgumentsHost) {
        const ctx = host.switchToHttp();
        const response = ctx.getResponse<Response>();
        const request = ctx.getRequest<Request>();
        const exceptionResponse = exception['response'];
        const status = exceptionResponse.status ?? exception.getStatus();
        const error = exceptionResponse.error ?? undefined;
        const message = exceptionResponse.message ?? undefined;
        console.error('HttpExceptionFilter: ', exception);

        response.status(status).json({
            status,
            error,
            message,
            timestamp: new Date().toISOString(),
            path: request.url,
        });
    }
}
