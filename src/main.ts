import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { ValidationPipe } from '@nestjs/common';
import { HttpExceptionFilter } from './errors/http-exception.filter';
import { GlobalExceptionFilter } from './errors/global-exception.filter';
import { configService } from './config/config.service';

async function bootstrap() {
    const appOptions = { cors: true };
    const app = await NestFactory.create(AppModule, appOptions);
    app.setGlobalPrefix('api');
    app.useGlobalPipes(new ValidationPipe());

    const options = new DocumentBuilder()
        .setTitle('Coding Social Network API')
        .setDescription('The Coding Social Network API')
        .setVersion('1.0')
        .build();

    const document = SwaggerModule.createDocument(app, options);
    SwaggerModule.setup('/docs', app, document);

    // global error handler
    app.useGlobalFilters(new GlobalExceptionFilter());
    app.useGlobalFilters(new HttpExceptionFilter());

    await app.listen(configService.getPort() || 3000);

    console.log('Server running on port ' + configService.getPort());
}

bootstrap();
