import { Injectable } from '@nestjs/common';
import { UsersService } from '../users/users.service';
import { JwtService } from '@nestjs/jwt';
import * as bcrypt from 'bcrypt';
import { GetUserDto } from '../users/DTO/get-user.dto';
import { Socket } from 'socket.io';
import { WsException } from '@nestjs/websockets';
import { User } from '../users/entities/user.entity';

@Injectable()
export class AuthService {
    constructor(
        private usersService: UsersService,
        private jwtService: JwtService,
    ) {}

    async validateUser(email: string, pass: string): Promise<GetUserDto> {
        const user = await this.usersService.findByEmail(email);
        const checkPassword = bcrypt.compareSync(pass, user.password);
        if (user && checkPassword) {
            return user.toDto();
        }
        return null;
    }

    async login(user: any) {
        const payload = { email: user.email, sub: user.id };
        const retrievedUser = await this.usersService.findByEmail(user.email);
        const jwtAccessToken = this.jwtService.sign(payload);
        return {
            access_token: jwtAccessToken,
            user: {
                email: user.email,
                id: user.id,
                firstName: retrievedUser.firstName,
                lastName: retrievedUser.lastName,
                fullName: retrievedUser.fullName,
                pseudo: retrievedUser.pseudo,
                password: '',
            },
        };
    }

    async getUserFromSocket(socket: Socket): Promise<User | null> {
        const authorizationHeader = socket.handshake.headers.authorization;
        if (!authorizationHeader) {
            return null;
        }
        const [, token] = authorizationHeader.split(' ');
        if (
            !token ||
            token === '' ||
            token === 'undefined' ||
            token === 'null'
        ) {
            return null;
        }
        const decoded = this.jwtService.decode(token);

        const user = await this.usersService.findById(decoded.sub);
        if (!user) {
            throw new WsException('Invalid credentials.');
        }
        return user;
    }
}
