import { Injectable } from '@nestjs/common';
import { MailerService } from '@nestjs-modules/mailer';
import { User } from '../users/entities/user.entity';

@Injectable()
export class MailService {
    constructor(private mailerService: MailerService) {}

    async sendUserResetPasswordMail(user: User, token: string) {
        const url = `localhost:3001/auth/reset-password/${token}`;

        await this.mailerService.sendMail({
            to: user.email,
            // from: '"Support Team" <support@example.com>', // override default from
            subject: 'Réinitialisation de votre mot de passe',
            template: 'forgot-password', // `.hbs` extension is appended automatically
            context: {
                fullName: `${user.fullName()}`,
                url,
            },
        });

        return {
            message: 'Mail sent successfully',
            url,
            token,
        };
    }
}
