import { Module } from '@nestjs/common';
import { MailService } from './mail.service';
import { MailerModule } from '@nestjs-modules/mailer';
import { join } from 'path';
import { HandlebarsAdapter } from '@nestjs-modules/mailer/dist/adapters/handlebars.adapter';
import { ConfigService } from '@nestjs/config';

@Module({
    imports: [
        MailerModule.forRootAsync({
            // imports: [ConfigModule], // import module if not enabled globally
            useFactory: async (config: ConfigService) => ({
                transport: {
                    host: config.get('MAILGUN_SMTP_SERVER'),
                    secure: false,
                    auth: {
                        user: config.get('MAILGUN_SMTP_LOGIN'),
                        pass: config.get('MAILGUN_SMTP_PASSWORD'),
                    },
                },
                defaults: {
                    from: `"Coding Social Network" <${config.get(
                        'MAIL_FROM',
                    )}>`,
                },
                template: {
                    dir: join(__dirname, 'templates'),
                    adapter: new HandlebarsAdapter(),
                    options: {
                        strict: true,
                    },
                },
            }),
            inject: [ConfigService],
        }),
    ],
    providers: [MailService],
    exports: [MailService],
})
export class MailModule {}
